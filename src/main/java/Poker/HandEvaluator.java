package Poker;

import java.util.Arrays;
import java.util.stream.IntStream;

class HandEvaluator {

  static boolean sameSuit(String string) {
    return Arrays.stream(string.split(" ")).map(c -> c.charAt(1)).distinct().count() == 1;
  }

  private static int[] toIntArray(String[] hand) {
    return Arrays.stream(hand).mapToInt(s -> {
      StringBuilder builder = new StringBuilder(s).deleteCharAt(1);
      if (builder.toString().equals("T")) {
        builder = new StringBuilder("10");
      } else if (builder.toString().equals("J")) {
        builder = new StringBuilder("11");
      } else if (builder.toString().equals("Q")) {
        builder = new StringBuilder("12");
      } else if (builder.toString().equals("K")) {
        builder = new StringBuilder("13");
      } else if (builder.toString().equals("A")) {
        builder = new StringBuilder("14");
      }
      return Integer.valueOf(builder.toString());
    }).toArray();
  }

  static boolean consecutive(String string) {
    int[] handInt = toIntArray(string.split(" "));
    Arrays.sort(handInt);
    return IntStream.range(1, handInt.length).allMatch(i -> handInt[i] - handInt[i - 1] == 1);
  }

  static boolean fromTen(String string) {
    int[] handInt = toIntArray(string.split(" "));
    Arrays.sort(handInt);
    return handInt[0] == 10;
  }

  static int[] sameValue(String string) {
    int[] handInt = toIntArray(string.split(" "));
    int[] occurence = new int[2];
    for (int i = 0; i < handInt.length; i++) {
      int n = i;
      int counter = (int) IntStream.range(i, handInt.length).filter(j -> handInt[n] == handInt[j]).count();
      if (counter > occurence[0]) {
        occurence[0] = counter;
        occurence[1] = handInt[i];
      }
    }
    return occurence;
  }

  static int pairs(String string) {
    int[] handInt = toIntArray(string.split(" "));
    return (int) Arrays.stream(handInt).distinct().count();
  }

  static int[] highest(String string) {
    int[] handInt = toIntArray(string.split(" "));
    int[] highests = new int[2];
    highests[0] = Arrays.stream(handInt).max().orElse(-1);
    highests[1] = Arrays.stream(handInt).filter(i -> i != highests[0]).max().orElse(-1);
    return highests;
  }
}

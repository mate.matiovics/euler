package Poker;

import java.util.HashMap;
import java.util.Objects;

public class PokerHelper {

  private static HashMap<String, Integer> evaluateHand(String hand) {
    HashMap<String, Integer> handValue = new HashMap<>();
    handValue.put("highest", HandEvaluator.highest(hand)[0]);
    handValue.put("2ndHighest", HandEvaluator.highest(hand)[1]);
    if (HandEvaluator.sameSuit(hand)) {
      handValue.put("score", HandEvaluator.consecutive(hand) ? (HandEvaluator.fromTen(hand) ? 10 : 9) : 6);
    } else {
      int[] numberOfSame = HandEvaluator.sameValue(hand);
      handValue.put("sameValue", numberOfSame[1]);
      if (numberOfSame[0] == 4) {
        handValue.put("score", 8);
      } else if (numberOfSame[0] == 3) {
        handValue.put("score", HandEvaluator.pairs(hand) == 2 ? 7 : 4);
      } else if (numberOfSame[0] == 2) {
        handValue.put("score", HandEvaluator.pairs(hand) == 3 ? 3 : 2);
      } else {
        handValue.put("score", HandEvaluator.consecutive(hand) ? 5 : 1);
      }
    }
    return handValue;
  }

  public static int compareHands(String hand1, String hand2) {
    HashMap<String, Integer> hand1Value = evaluateHand(hand1);
    HashMap<String, Integer> hand2Value = evaluateHand(hand2);
    if (hand1Value.get("score") > hand2Value.get("score")) {
      return 1;
    } else if (Objects.equals(hand1Value.get("score"), hand2Value.get("score"))) {
      int score = hand1Value.get("score");
      if (score == 2 || score == 3 || score == 4 || score == 7 || score == 8) {
        if (hand1Value.get("sameValue") > hand2Value.get("sameValue")) {
          return 1;
        } else if (Objects.equals(hand1Value.get("sameValue"), hand2Value.get("sameValue"))) {
          if (hand1Value.get("highest") > hand2Value.get("highest")) {
            return 1;
          } else if (Objects.equals(hand1Value.get("highest"), hand2Value.get("highest"))) {
            return hand1Value.get("2ndHighest") > hand2Value.get("2ndHighest") ? 1 : 2;
          } else {
            return 2;
          }
        } else {
          return 2;
        }
      } else {
        if (hand1Value.get("highest") > hand2Value.get("highest")) {
          return 1;
        } else if (Objects.equals(hand1Value.get("highest"), hand2Value.get("highest"))) {
          return hand1Value.get("2ndHighest") > hand2Value.get("2ndHighest") ? 1 : 2;
        } else {
          return 2;
        }
      }
    } else {
      return 2;
    }
  }
}

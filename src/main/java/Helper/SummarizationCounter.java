package Helper;

import java.util.List;

public class SummarizationCounter {
  public long counter = 0;
  public long primeCounter = 0;

  public SummarizationCounter() {
  }

  public void countSummarization(int previous, int n) {
    int i = previous - 1;
    for (int k = 0; k <= n / i; k++) {
      int remainder = n - k * i;
      if (i == 2) {
        counter++;
      } else {
        countSummarization(i, remainder);
      }
    }
  }

  public void countPrimeSummarization(List<Integer> primes, int previous, int n) {
    int index = previous - 1;
    int i = primes.get(index);
    for (int k = 0; k <= n / i; k++) {
      int remainder = n - k * i;
      if (i == 3 && remainder % 2 == 0) {
        primeCounter++;
      } else if (i > 3) {
        countPrimeSummarization(primes, index, remainder);
      }
    }
  }
}

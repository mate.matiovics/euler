package Helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Permuter {
  private List<String> permutations = new ArrayList<>();
  private List<int[]> arrayPermutations = new ArrayList<>();

  public Permuter() {
  }

  public <T> void permute(T input) {
    char[] array = String.valueOf(input).toCharArray();
    doPermute(array, 0);
  }

  public void permuteIndices(int[] indices) {
    doPermuteArray(Arrays.copyOf(indices, indices.length), 0);
  }

  private void doPermute(char[] array, int n) {
    if (n == array.length - 1) {
      permutations.add(new String(array));
    } else {
      for (int i = n; i < array.length; i++) {
        char tmp = array[i];
        array[i] = array[n];
        array[n] = tmp;
        doPermute(array, n + 1);
        char tmp2 = array[i];
        array[i] = array[n];
        array[n] = tmp2;
      }
    }
  }

  private void doPermuteArray(int[] array, int n) {
    if (n == array.length - 1) {
      arrayPermutations.add(Arrays.copyOf(array, array.length));
    } else {
      for (int i = n; i < array.length; i++) {
        int tmp = array[i];
        array[i] = array[n];
        array[n] = tmp;
        doPermuteArray(array, n + 1);
        int tmp2 = array[i];
        array[i] = array[n];
        array[n] = tmp2;
      }
    }
  }

  public List<String> getPermutations() {
    return permutations;
  }

  public List<int[]> getPermutedIndices() {
    return arrayPermutations;
  }
}

package Helper;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

public class Interpreter {
  private LinkedList<Character> rpn = new LinkedList<>();
  private Stack<Character> operators = new Stack<>();
  private Stack<Double> values = new Stack<>();
  private Map<Character, Integer> precedences;

  public Interpreter() {
    precedences = new HashMap<>();
    precedences.put('*', 3);
    precedences.put('/', 3);
    precedences.put('+', 2);
    precedences.put('-', 2);
  }

  public void parse(String expression) {
    char[] chars = expression.toCharArray();
    for (char c : chars) {
      if (c == '(') {
        operators.push(c);
      } else if (c == ')') {
        while (operators.peek() != '(') {
          rpn.add(operators.pop());
        }
        if (operators.empty()) {
          throw new IllegalArgumentException("Mismatching parentheses!");
        }
        operators.pop();
      } else if (c == '*' || c == '/' || c == '-' || c == '+') {
        while (!operators.empty() && operators.peek() != '(' && precedences.get(operators.peek()) >= precedences.get(c)) {
          rpn.add(operators.pop());
        }
        operators.push(c);
      } else {
        rpn.add(c);
      }
    }
    while (!operators.empty()) {
      rpn.add(operators.pop());
    }
  }

  public Double evaluate() {
    while (!rpn.isEmpty()) {
      Character c = rpn.poll();
      if (c == '*' || c == '/' || c == '-' || c == '+') {
        double val1 = values.pop();
        double val2 = values.pop();
        if (c == '*') {
          values.push(val1 * val2);
        } else if (c == '/') {
          values.push(val2 / val1);
        } else if (c == '+') {
          values.push(val1 + val2);
        } else {
          values.push(val2 - val1);
        }
      } else {
        values.push(Double.parseDouble(String.valueOf(c)));
      }
    }
    return values.pop();
  }
}

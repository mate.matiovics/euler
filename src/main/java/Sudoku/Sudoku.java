package Sudoku;

import Helper.Permuter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

//TODO: refactor
public class Sudoku {
  private int[][] board;
  private Map<Integer, Line> lines;
  private Map<Integer, Column> columns;
  private Map<Integer, Block> blocks;
  private List<Element> elements;

  public Sudoku(String[] input) {
    lines = new HashMap<>();
    columns = new HashMap<>();
    blocks = new HashMap<>();
    board = new int[9][9];
    for (int i = 0; i < input.length; i++) {
      int[] temp = new int[input[i].length()];
      for (int j = 0; j < temp.length; j++) {
        temp[j] = input[i].charAt(j) - '0';
      }
      board[i] = Arrays.copyOf(temp, temp.length);
    }
    fillBoard();
  }

  private void fillBoard() {
    lines = new HashMap<>();
    columns = new HashMap<>();
    blocks = new HashMap<>();
    for (int i = 0; i < 9; i++) {
      lines.put(i, new Line(i, board[i]));
      int[] column = new int[9];
      int[] block = new int[9];
      for (int j = 0; j < 9; j++) {
        column[j] = board[j][i];
        if (j < 3) {
          for (int k = 0; k < 3; k++) {
            block[j * 3 + k] = board[i / 3 * 3 + j][i % 3 * 3 + k];
          }
        }
      }
      columns.put(i, new Column(i, column));
      blocks.put(i, new Block(i, block));
    }
    elements = new ArrayList<>();
    elements.addAll(lines.values());
    elements.addAll(columns.values());
    elements.addAll(blocks.values());
  }

  public void solve() {
    deduceMissingNumbers();
    Element mostComplete = findMostCompleteElement();
    int[] backup = Arrays.copyOf(mostComplete.getValues(), mostComplete.getValues().length);

    TreeSet<Integer> missing = mostComplete.getMissing();
    StringBuilder b = new StringBuilder();
    for (Integer i : missing) {
      b.append(String.valueOf(i));
    }

    Permuter permuter = new Permuter();
    permuter.permute(b.toString());
    List<String> numberCombinations = permuter.getPermutations();

    for (String combination : numberCombinations) {
      updateElement(mostComplete, combination);
      if (valid()) {
        solve();
      }
      if (isSolved()) {
        break;
      }
      mostComplete.setValues(Arrays.copyOf(backup, backup.length));
      mostComplete.calculate();
      mostComplete.update();
    }
  }

  private void deduceMissingNumbers() {
    elements.forEach(Element::calculate);
  }

  private boolean isSolved() {
    for (Line line : lines.values()) {
      for (int i : line.values) {
        if (i == 0) {
          return false;
        }
      }
    }
    return true;
  }

  private boolean valid() {
    return elements.stream().allMatch(this::isValidElement);
  }

  private boolean isValidElement(Element element) {
    boolean[] marked = new boolean[10];
    for (Integer i : element.values) {
      if (marked[i] && i != 0) {
        return false;
      }
      marked[i] = true;
    }
    return true;
  }

  private void updateElement(Element element, String string) {
    int i = 0;
    for (int j = 0; j < element.getValues().length; j++) {
      if (element.getValues()[j] == 0) {
        element.getValues()[j] = string.charAt(i++) - '0';
      }
    }
    element.calculate();
    element.update();
  }

  private Element findMostCompleteElement() {
    Element best = lines.get(0);
    int min = Integer.MAX_VALUE;

    for (Element element : elements) {
      if (element.missing.size() < min && element.missing.size() != 0) {
        min = element.missing.size();
        best = element;
      }
    }

    return best;
  }

  public String toString() {
    StringBuilder b = new StringBuilder();
    for (Line line : lines.values()) {
      b.append(Arrays.toString(line.values)).append("\n");
    }
    return b.toString();
  }

  public int[] getFirstThreeNumbers() {
    return Arrays.copyOf(lines.get(0).values, 3);
  }

  private abstract class Element {
    int id;
    int[] values;
    TreeSet<Integer> missing;
    TreeSet<Integer> found;

    Element(int id, int[] values) {
      this.id = id;
      this.values = values;
      missing = new TreeSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
      found = new TreeSet<>();
      for (int value : values) {
        if (value != 0) {
          found.add(value);
        }
      }
      missing.removeAll(found);
    }

    abstract void update();

    void calculate() {
      missing = new TreeSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
      found = new TreeSet<>();
      for (int value : values) {
        if (value != 0) {
          found.add(value);
        }
      }
      missing.removeAll(found);
    }

    int[] getValues() {
      return values;
    }

    TreeSet<Integer> getMissing() {
      return missing;
    }

    void setValues(int[] values) {
      this.values = values;
    }

    public String toString() {
      return Arrays.toString(values);
    }
  }

  private class Line extends Element {

    Line(int id, int[] values) {
      super(id, values);
    }

    public void solve() {
      calculate();
      Map<Integer, TreeSet> used = new HashMap<>();
      for (int j = 0; j < values.length; j++) {
        if (values[j] == 0) {
          TreeSet<Integer> values = new TreeSet<>(found);
          values.addAll(columns.get(j).found);
          values.addAll(blocks.get(id / 3 * 3 + j / 3).found);
          used.put(j, values);
        }
      }
      for (int i = 0; i < used.size(); i++) {
        for (int k : used.keySet()) {
          TreeSet<Integer> missingVals = new TreeSet<>(missing);
          missingVals.removeAll(used.get(k));
          if (missingVals.size() == 1) {
            int num = missingVals.first();
            used.get(k).add(num);
            values[k] = num;
            calculate();
            break;
          }
        }
      }
      update();
    }

    public void update() {
      for (int i = 0; i < values.length; i++) {
        columns.get(i).values[id] = values[i];
        blocks.get(id / 3 * 3 + i / 3).values[id % 3 * 3 + i % 3] = values[i];
      }
    }

  }

  private class Column extends Element {

    Column(int id, int[] values) {
      super(id, values);
    }

    public void solve() {
      calculate();
      Map<Integer, TreeSet> used = new HashMap<>();
      for (int j = 0; j < values.length; j++) {
        if (values[j] == 0) {
          TreeSet<Integer> values = new TreeSet<>(found);
          values.addAll(lines.get(j).found);
          values.addAll(blocks.get(j / 3 * 3 + id / 3).found);
          used.put(j, values);
        }
      }
      for (int i = 0; i < used.size(); i++) {
        for (int k : used.keySet()) {
          TreeSet<Integer> missingVals = new TreeSet<>(missing);
          missingVals.removeAll(used.get(k));
          if (missingVals.size() == 1) {
            int num = missingVals.first();
            used.get(k).add(num);
            values[k] = num;
            calculate();
            break;
          }
        }
      }
      update();
    }

    public void update() {
      for (int i = 0; i < values.length; i++) {
        lines.get(i).values[id] = values[i];
        blocks.get(i / 3 * 3 + id / 3).values[i % 3 * 3 + id % 3] = values[i];
      }
    }
  }

  private class Block extends Element {

    Block(int id, int[] values) {
      super(id, values);
    }

    public void solve() {
      calculate();
      Map<Integer, TreeSet> used = new HashMap<>();
      for (int j = 0; j < values.length; j++) {
        if (values[j] == 0) {
          TreeSet<Integer> values = new TreeSet<>(found);
          values.addAll(lines.get(id / 3 * 3 + j / 3).found);
          values.addAll(columns.get(id % 3 * 3 + j % 3).found);
          used.put(j, values);
        }
      }
      for (int i = 0; i < used.size(); i++) {
        for (int k : used.keySet()) {
          TreeSet<Integer> missingVals = new TreeSet<>(missing);
          missingVals.removeAll(used.get(k));
          if (missingVals.size() == 1) {
            int num = missingVals.first();
            used.get(k).add(num);
            values[k] = num;
            calculate();
            break;
          }
        }
      }
      update();
    }

    public void update() {
      for (int i = 0; i < values.length; i++) {
        lines.get(id / 3 * 3 + i / 3).values[id % 3 * 3 + i % 3] = values[i];
        columns.get(id % 3 * 3 + i % 3).values[id / 3 * 3 + i / 3] = values[i];
      }
    }
  }
}

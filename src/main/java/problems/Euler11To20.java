package problems;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static Helper.HelperClass.factorial;
import static Helper.HelperClass.getCollatzCount;
import static Helper.HelperClass.numberOfDivisors;
import static Helper.HelperClass.sumMapValues;
import static Helper.HelperClass.sumOfDigits;
import static java.math.BigInteger.valueOf;

class Euler11To20 {

  static long problem11() {
    String string = ("08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08\n" +
        "49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00\n" +
        "81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65\n" +
        "52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91\n" +
        "22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80\n" +
        "24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50\n" +
        "32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70\n" +
        "67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21\n" +
        "24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72\n" +
        "21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95\n" +
        "78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92\n" +
        "16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57\n" +
        "86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58\n" +
        "19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40\n" +
        "04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66\n" +
        "88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69\n" +
        "04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36\n" +
        "20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16\n" +
        "20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54\n" +
        "01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48")
        .replaceAll(" ", "").replaceAll("\n", "");
    int[][] array = new int[20][20];
    for (int i = 0; i < 800; i += 2) {
      array[i / 2 / 20][i / 2 % 20] = Integer.valueOf(string.substring(i, i + 2));
    }
    long product = 0;
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        TreeSet<Integer> setOfProducts = new TreeSet<>();
        try {
          setOfProducts.add(array[i][j] * array[i + 1][j + 1] * array[i + 2][j + 2] * array[i + 3][j + 3]);
          setOfProducts.add(array[i][j] * array[i + 1][j - 1] * array[i + 2][j - 2] * array[i + 3][j - 3]);
          setOfProducts.add(array[i][j] * array[i][j + 1] * array[i][j + 2] * array[i][j + 3]);
          setOfProducts.add(array[i][j] * array[i + 1][j] * array[i + 2][j] * array[i + 3][j]);
          if (product < setOfProducts.last()) {
            product = setOfProducts.last();
          }
        } catch (Exception ignored) {
        }
      }
    }
    return product;
  }

  static long problem12() {
    long counter = 1;
    long i = 1;
    while (counter < 500) {
      i++;
      counter = numberOfDivisors(i * (i + 1) / 2);
    }
    return i * (i + 1) / 2;
  }

  static BigInteger problem13() throws IOException, URISyntaxException {
    List<String> stringList = Files.lines(Paths.get(Euler11To20.class.getResource("/problem13.txt").toURI())).collect(Collectors.toList());
    BigInteger sum = valueOf(0);
    for (String number : stringList) {
      sum = sum.add(new BigInteger(number));
    }
    while (sum.compareTo(valueOf(9999999999L)) == 1) {
      sum = sum.divide(valueOf(10));
    }
    return sum;
  }

  static long problem14() {
    long maxCounter = 0;
    long number = 1;
    for (long i = 1; i < 1000000; i++) {
      long steps = getCollatzCount(0, i);
      if (steps > maxCounter) {
        maxCounter = steps;
        number = i;
      }
    }
    return number;
  }

  static long problem15() {
    long[][] matrix = new long[21][21];
    for (int i = 0; i < 21; i++) {
      matrix[i][20] = 1;
    }
    for (int j = 0; j < 21; j++) {
      matrix[20][j] = 1;
    }
    for (int j = 19; j >= 0; j--) {
      for (int i = 19; i >= 0; i--) {
        matrix[i][j] = matrix[i + 1][j] + matrix[i][j + 1];
      }
    }
    return matrix[0][0];
  }

  static long problem16() {
    BigInteger number = valueOf(2);
    String string = String.valueOf(number.pow(1000));
    return string.chars().mapToLong(Character::getNumericValue).sum();
  }

  //TODO: make it shorter somehow
  static int problem17() {
    HashMap<Integer, Integer> oneToNine = new HashMap<>();
    oneToNine.put(1, "one".length());
    oneToNine.put(2, "two".length());
    oneToNine.put(3, "three".length());
    oneToNine.put(4, "four".length());
    oneToNine.put(5, "five".length());
    oneToNine.put(6, "six".length());
    oneToNine.put(7, "seven".length());
    oneToNine.put(8, "eight".length());
    oneToNine.put(9, "nine".length());
    HashMap<Integer, Integer> tenToNineteen = new HashMap<>();
    tenToNineteen.put(10, "ten".length());
    tenToNineteen.put(11, "eleven".length());
    tenToNineteen.put(12, "twelve".length());
    tenToNineteen.put(13, "thirteen".length());
    tenToNineteen.put(14, "fourteen".length());
    tenToNineteen.put(15, "fifteen".length());
    tenToNineteen.put(16, "sixteen".length());
    tenToNineteen.put(17, "seventeen".length());
    tenToNineteen.put(18, "eighteen".length());
    tenToNineteen.put(19, "nineteen".length());
    HashMap<Integer, Integer> tens = new HashMap<>();
    tens.put(20, "twenty".length());
    tens.put(30, "thirty".length());
    tens.put(40, "forty".length());
    tens.put(50, "fifty".length());
    tens.put(60, "sixty".length());
    tens.put(70, "seventy".length());
    tens.put(80, "eighty".length());
    tens.put(90, "ninety".length());
    int hundred = "hundred".length();
    int and = "and".length();
    int thousand = "onethousand".length();
    return 10 * (sumMapValues(oneToNine) + sumMapValues(tenToNineteen) + 10 * sumMapValues(tens) + 8 * sumMapValues(oneToNine))
        + 100 * sumMapValues(oneToNine) + 9 * hundred + 9 * 99 * (hundred + and) + thousand;
  }

  static int problem18() throws URISyntaxException, IOException {
    String triangle = new String(Files.readAllBytes(Paths.get(Euler11To20.class.getResource("/problem18.txt").toURI())));
    String[] lines = triangle.split("\r\n");
    int[][] matrix = new int[lines.length][lines.length];
    for (int i = 0; i < lines.length; i++) {
      String[] line = lines[i].split(" ");
      for (int j = 0; j < line.length; j++) {
        matrix[i][j] = Integer.valueOf(line[j]);
      }
    }

    for (int i = matrix.length - 2; i >= 0; i--) {
      for (int j = 0; j < i + 1; j++) {
        if (matrix[i + 1][j] > matrix[i + 1][j + 1]) {
          matrix[i][j] = matrix[i][j] + matrix[i + 1][j];
        } else {
          matrix[i][j] = matrix[i][j] + matrix[i + 1][j + 1];
        }
      }
    }
    return matrix[0][0];
  }

  static int problem19() {
    int counter = 0;
    for (int i = 1901; i < 2001; i++) {
      for (int j = 1; j < 13; j++) {
        if (LocalDate.of(i, j, 1).getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
          counter++;
        }
      }
    }
    return counter;
  }

  static long problem20() {
    return sumOfDigits(factorial(BigInteger.valueOf(100)));
  }
}

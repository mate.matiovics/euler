package problems;

import Helper.HelperClass;
import Helper.SummarizationCounter;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static Helper.HelperClass.contdFractions;
import static Helper.HelperClass.getConvergent;
import static Helper.HelperClass.getTotient;
import static Helper.HelperClass.greatestCommonDivisor;
import static Helper.HelperClass.sumOfFactorial;
import static java.lang.Character.getNumericValue;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static java.math.BigInteger.valueOf;

class Euler71To80 {

  static long problem71() {
    double lowerBound = 2.0 / 5.0;
    double upperBound = 3.0 / 7.0;
    int numerator = 2;
    int denominator = 5;
    for (int i = 2; i < 1000000; i++) {
      for (int j = numerator * (i / denominator); j < i; j++) {
        double division = (double) j / i;
        if (division < upperBound && division > lowerBound) {
          if (greatestCommonDivisor(new long[] {i, j}) == 1) {
            numerator = j;
            denominator = i;
          }
        } else if (division > upperBound) {
          break;
        }
      }
    }
    return numerator;
  }

  static long problem72() {
    long counter = 0;
    for (long i = 2; i <= 1000000; i++) {
      counter += getTotient(i);
    }
    return counter;
  }

  static long problem73() {
    double lowerBound = 1.0 / 3.0;
    double upperBound = 0.5;
    int counter = 0;
    for (int i = 2; i <= 12000; i++) {
      for (int j = 1; j < i; j++) {
        double division = (double) j / (double) i;
        if (division < upperBound && division > lowerBound) {
          if (greatestCommonDivisor(new long[] {i, j}) == 1) {
            counter++;
          }
        } else if (division > upperBound) {
          break;
        }
      }
    }
    return counter;
  }

  static long problem74() {
    int counter = 0;
    for (int i = 1; i < 1000000; i++) {
      List<Integer> chain = new ArrayList<>();
      chain.add(i);
      int sum = sumOfFactorial(i);
      while (!chain.contains(sum)) {
        chain.add(sum);
        sum = sumOfFactorial(sum);
      }
      if (chain.size() == 60) {
        counter++;
      }
    }
    return counter;
  }

  static long problem75() {
    Set<Integer> set = new TreeSet<>();
    Set<Integer> repeating = new TreeSet<>();
    for (int m = 1; m <= Math.sqrt(750000); m++) {
      int a = m % 2 != 0 ? 2 : 1;
      for (int n = a; n < m; n += 2) {
        if (greatestCommonDivisor(new long[] {m, n}) == 1) {
          int k = 1;
          int product = k * m * (m + n);
          if (product > 750000) {
            break;
          }
          while (product <= 750000) {
            if (set.contains(2 * product)) {
              repeating.add(2 * product);
            }
            set.add(2 * product);
            k++;
            product = k * m * (m + n);
          }
        }
      }
    }
    set.removeAll(repeating);
    return set.size();
  }

  static long problem76() {
    SummarizationCounter sumCounter = new SummarizationCounter();
    sumCounter.countSummarization(100, 100);
    return sumCounter.counter;
  }

  static int problem77() {
    int value = 10;
    long counter = 5;
    while (counter <= 5000) {
      value++;
      List<Integer> primes = IntStream.range(2, value).filter(HelperClass::isPrime).boxed()
          .collect(Collectors.toList());
      SummarizationCounter sumCounter = new SummarizationCounter();
      sumCounter.countPrimeSummarization(primes, primes.size(), value);
      counter = sumCounter.primeCounter;
    }
    return value;
  }

  //integer partition
  static int problem78() {
    List<BigInteger> sums = new ArrayList<>();
    sums.add(ONE);
    int number = 1;
    while (true) {
      int i = 1;
      int k = 1;
      sums.add(ZERO);
      while (k <= number) {
        int prefix = (i % 4 == 1 || i % 4 == 2) ? 1 : -1;
        sums.set(number, sums.get(number).add(sums.get(number - k).multiply(valueOf(prefix))));
        i++;
        int mPrefix = (i % 2 == 1) ? 1 : -1;
        int m = (i + 1) / 2;
        k = mPrefix * m * (3 * mPrefix * m - 1) / 2;
      }
      if (sums.get(number).remainder(valueOf(1000000)).equals(ZERO)) {
        break;
      }
      number++;
    }
    return number;
  }

  static String problem79() throws URISyntaxException, IOException {
    List<Set<Integer>> before = new ArrayList<>();
    List<Set<Integer>> after = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      before.add(new TreeSet<>());
      after.add(new TreeSet<>());
    }

    String content = new String(Files.readAllBytes(Paths.get(Euler71To80.class.getResource("/problem79.txt").toURI())));
    String[] codes = content.split("\n");
    TreeSet<Integer> possibleDigits = new TreeSet<>();
    for (String code : codes) {
      int first = getNumericValue(code.charAt(0));
      int second = getNumericValue(code.charAt(1));
      int third = getNumericValue(code.charAt(2));
      possibleDigits.add(first);
      possibleDigits.add(second);
      possibleDigits.add(third);
      after.get(first).add(second);
      after.get(first).add(third);
      before.get(second).add(first);
      after.get(second).add(third);
      before.get(third).add(first);
      before.get(third).add(second);
    }

    Map<Integer, Integer> result = new TreeMap<>();
    for (int i = 0; i < before.size(); i++) {
      TreeSet<Integer> digits = new TreeSet<>(possibleDigits);
      Set<Integer> prefix = before.get(i);
      Set<Integer> suffix = after.get(i);
      int index = prefix.size();
      digits.removeAll(prefix);
      digits.removeAll(suffix);
      if (digits.size() == 1) {
        result.put(index, digits.first());
      }
    }
    StringBuilder b = new StringBuilder();
    for (Integer integer : result.keySet()) {
      b.append(String.valueOf(result.get(integer)));
    }
    return b.toString();
  }

  static long problem80() {
    long sum = 0;
    for (int i = 0; i < 100; i++) {
      if (Math.sqrt(i) % 1.0 != 0.0) {
        List<Integer> list = contdFractions(i, (int) Math.sqrt(i), new ArrayList<>(), 1, 0, 1000);
        BigInteger[] array = getConvergent(list, list.size(), i);
        BigDecimal sqrt = new BigDecimal(array[0]).divide(new BigDecimal(array[1]), 1000, BigDecimal.ROUND_HALF_DOWN);
        for (Character digit : sqrt.toString().substring(2, 101).toCharArray()) {
          sum += getNumericValue(digit);
        }
        sum += getNumericValue(sqrt.toString().charAt(0));
      }
    }
    return sum;
  }
}

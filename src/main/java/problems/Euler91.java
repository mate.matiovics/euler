package problems;

import Helper.HelperClass;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

class Euler91 {

  static long solve() {
    int grid = 50;
    List<Point> list = new ArrayList<>();
    for (int i = 0; i <= grid; i++) {
      for (int j = 0; j <= grid; j++) {
        if (!(j == 0 && i == 0)) {
          list.add(new Point(i, j));
        }
      }
    }
    int result = 0;
    for (int i = 0; i < list.size(); i++) {
      Point point = list.get(i);
      for (int j = i + 1; j < list.size(); j++) {
        Point point1 = list.get(j);
        if (!point.equals(point1)) {
          long[] slope1 = point.slopeToO;
          long[] slope2 = point1.slopeToO;
          long[] slope3 = point.slopeTo(point1);
          if (arePerpendicular(slope1, slope2) || arePerpendicular(slope1, slope3) || arePerpendicular(slope2, slope3)) {
            result++;
          }
        }
      }
    }
    return result;
  }

  private static boolean arePerpendicular(long[] slope1, long[] slope2) {
    if ((slope1[0] == 0 && slope2[1] == 0) || (slope1[1] == 0 && slope2[0] == 0)) {
      return true;
    }
    boolean slope1Pos = (slope1[0] > 0 && slope1[1] > 0) || (slope1[0] < 0 && slope1[1] < 0);
    boolean slope2Pos = (slope2[0] > 0 && slope2[1] > 0) || (slope1[0] < 0 && slope2[1] < 0);
    if (slope1Pos == slope2Pos) {
      return false;
    }
    return abs(slope1[0]) == abs(slope2[1]) && abs(slope2[0]) == abs(slope1[1]);
  }

  private static class Point {
    int x;
    int y;
    long[] slopeToO;

    Point(int x, int y) {
      this.x = x;
      this.y = y;
      if (x == 0) {
        slopeToO = new long[] {1, 0};
      } else if (y == 0) {
        slopeToO = new long[] {0, 1};
      } else {
        long[] distances = {y, x};
        long gCd = HelperClass.greatestCommonDivisor(distances);
        slopeToO = new long[] {y / gCd, x / gCd};
      }
    }

    long[] slopeTo(Point p) {
      int horizontal = this.x - p.x;
      int vertical = this.y - p.y;
      long[] slopeTo;
      if (horizontal == 0) {
        slopeTo = new long[] {1, 0};
      } else if (vertical == 0) {
        slopeTo = new long[] {0, 1};
      } else {
        long[] distances = {horizontal, vertical};
        long gCd = HelperClass.greatestCommonDivisor(distances);
        slopeTo = new long[] {vertical / gCd, horizontal / gCd};
      }
      return slopeTo;
    }

    boolean equals(Point that) {
      return this.x == that.x && this.y == that.y;
    }
  }
}

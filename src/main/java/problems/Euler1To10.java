package problems;

import Helper.HelperClass;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static Helper.HelperClass.getPrimeFactors;
import static Helper.HelperClass.isPalindrome;
import static Helper.HelperClass.isPrime;
import static java.lang.Math.abs;

class Euler1To10 {

  static int problem1() {
    return IntStream.range(1, 1000).filter(number -> (number % 3 == 0 || number % 5 == 0)).sum();
  }

  static int problem2() {
    int prev = 1;
    int actual = 2;
    int sum = 2;
    while (actual <= 4000000) {
      int temp = actual;
      actual = prev + actual;
      prev = temp;
      if (actual % 2 == 0) {
        sum += actual;
      }
    }
    return sum;
  }

  static long problem3() {
    for (long i = 2; i < 600851475143L / 2; i++) {
      if (600851475143L % i == 0) {
        if (isPrime(600851475143L / i)) {
          return 600851475143L / i;
        }
      }
    }
    return -1;
  }

  static int problem4() {
    int answer = 0;
    for (int i = 999; i > 99; i--) {
      for (int j = i; j > 99; j--) {
        if (i * j > answer && isPalindrome(i * j)) {
          answer = i * j;
        }
      }
    }
    return answer;
  }

  static int problem5() {
    Map<Long, Integer> allPrimeFactors = new HashMap<>();

    for (int i = 2; i <= 20; i++) {
      HashMap<Long, Integer> primeFactors = getPrimeFactors(i);
      for (Long factor : primeFactors.keySet()) {
        if (!allPrimeFactors.containsKey(factor) || allPrimeFactors.get(factor) < primeFactors.get(factor)) {
          allPrimeFactors.put(factor, primeFactors.get(factor));
        }
      }
    }
    return allPrimeFactors.keySet().stream()
        .mapToInt(k -> ((int) Math.pow(k, allPrimeFactors.get(k))))
        .reduce(1, Math::multiplyExact);
  }

  static long problem6() {
    long sumOfSquares = 0;
    long sum = 0;
    for (long i = 1; i < 101; i++) {
      sum += i;
      sumOfSquares += i * i;
    }
    return abs(sumOfSquares - sum * sum);
  }

  static long problem7() {
    long counter = 0;
    long i = 2;
    while (counter < 10001) {
      if (isPrime(i)) {
        counter++;
      }
      i++;
    }
    return i - 1;
  }

  static long problem8() {
    String numberAsString = ("73167176531330624919225119674426574742355349194934\n" +
        "96983520312774506326239578318016984801869478851843\n" +
        "85861560789112949495459501737958331952853208805511\n" +
        "12540698747158523863050715693290963295227443043557\n" +
        "66896648950445244523161731856403098711121722383113\n" +
        "62229893423380308135336276614282806444486645238749\n" +
        "30358907296290491560440772390713810515859307960866\n" +
        "70172427121883998797908792274921901699720888093776\n" +
        "65727333001053367881220235421809751254540594752243\n" +
        "52584907711670556013604839586446706324415722155397\n" +
        "53697817977846174064955149290862569321978468622482\n" +
        "83972241375657056057490261407972968652414535100474\n" +
        "82166370484403199890008895243450658541227588666881\n" +
        "16427171479924442928230863465674813919123162824586\n" +
        "17866458359124566529476545682848912883142607690042\n" +
        "24219022671055626321111109370544217506941658960408\n" +
        "07198403850962455444362981230987879927244284909188\n" +
        "84580156166097919133875499200524063689912560717606\n" +
        "05886116467109405077541002256983155200055935729725\n" +
        "71636269561882670428252483600823257530420752963450")
        .replaceAll("\n", "");

    long max = 0;
    for (int i = 0; i < numberAsString.length() - 13; i++) {
      long actualProduct = 1;
      for (int j = 0; j < 13; j++) {
        actualProduct *= Character.getNumericValue(numberAsString.charAt(i + j));
      }
      if (actualProduct > max) {
        max = actualProduct;
      }
    }
    return max;
  }

  static int problem9() {
    for (int i = 1; i < 1000; i++) {
      for (int j = i + 1; j < 1000; j++) {
        int k = 1000 - i - j;
        if (i * i + j * j == k * k) {
          return i * j * (1000 - i - j);
        }
      }
    }
    return -1;
  }

  static long problem10() {
    return LongStream.range(1, 2000000).filter(HelperClass::isPrime).sum();
  }
}

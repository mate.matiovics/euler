package problems;

import java.math.BigInteger;

public class Euler97 {

  public static String solve() {
    BigInteger two = BigInteger.valueOf(2);
    BigInteger result = two.pow(7830457).multiply(BigInteger.valueOf(28433)).add(BigInteger.ONE);
    String stringResult = result.toString();
    return stringResult.substring(stringResult.length() - 10);
  }
}

package problems;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static Helper.HelperClass.contdFractionsPeriod;
import static Helper.HelperClass.getConvergent;
import static java.math.BigInteger.valueOf;

public class Euler100 {

  public static long solve() {
    BigInteger P = valueOf(164);
    BigInteger Q = valueOf(58);
    int d = 8;
    BigInteger D = valueOf(d);
    List<Integer> list = contdFractionsPeriod((int) Math.sqrt(d), d, (int) Math.sqrt(d), new ArrayList<>(), 1);
    int counter = 1;
    while (true) {
      BigInteger[] array = getConvergent(list, counter, d);
      BigInteger x = array[0];
      BigInteger y = array[1];
      if (x.pow(2).subtract(y.pow(2).multiply(valueOf(d))).equals(BigInteger.ONE)) {
        BigInteger p = P.multiply(x).add(D.multiply(Q).multiply(y));
        BigInteger q = P.multiply(y).add(Q.multiply(x));
        BigInteger result = p.add(valueOf(4)).divide(valueOf(8));
        if (result.compareTo(valueOf(1000000000000L)) >= 0) {
          return q.add(valueOf(2)).divide(valueOf(4)).longValue();
        }
      }
      counter++;
    }
  }
}

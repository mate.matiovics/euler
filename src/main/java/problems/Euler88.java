package problems;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.IntStream;

import static Helper.HelperClass.getPrimeFactors;

class Euler88 {

  static int solve() {
    int maxFactorNo = maxFactorNo(2 * 12000);
    Map<Integer, Integer> map = new TreeMap<>();
    IntStream.range(2, maxFactorNo + 1).forEach(i -> {
      Factors factors = new Factors(i, 2 * 12000, map);
      factors.generate();
    });
    return map.keySet().stream().mapToInt(map::get).distinct().sum();
  }

  private static int maxFactorNo(int n) {
    int max = 0;
    for (int i = 2; i <= n; i++) {
      HashMap<Long, Integer> primeFactors = getPrimeFactors(i);
      int factorNo = primeFactors.keySet().stream().mapToInt(primeFactors::get).sum();
      if (factorNo > max) {
        max = factorNo;
      }
    }
    return max;
  }

  private static class Factors {
    private int[] array;
    private int size;
    private int product;
    private Map<Integer, Integer> map;

    Factors(int size, int product, Map<Integer, Integer> map) {
      this.map = map;
      this.size = size;
      this.product = product;
      this.array = new int[size];
      for (int i = 0; i < size; i++) {
        array[i] = 2;
      }
      updateMap();
    }

    void generate() {
      int i = size - 1;
      int max = (int) Math.pow(product / Math.pow(2, i), 1.0 / (size - i));
      while (i >= 0) {
        int val = array[i];
        if (val < max) {
          while (i < array.length) {
            array[i++] = val + 1;
          }
          updateMap();
          max = product / reduce(0, --i);
        }
        while (array[i] < max) {
          array[i]++;
          updateMap();
        }
        i--;
      }
    }

    private int reduce(int min, int max) {
      int product = 1;
      for (int i = min; i < max; i++) {
        product *= array[i];
      }
      return product;
    }

    private int reduce() {
      return Arrays.stream(array).reduce(1, Math::multiplyExact);
    }

    private int sum() {
      return Arrays.stream(array).sum();
    }

    private void updateMap() {
      int reduce = reduce();
      int sum = sum();
      int k = array.length + reduce - sum;
      if (k <= product / 2) {
        if (!map.containsKey(k) || map.get(k) > reduce) {
          map.put(k, reduce);
        }
      }
    }
  }
}

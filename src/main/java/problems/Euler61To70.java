package problems;

import Helper.Permuter;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static Helper.HelperClass.arePermutations;
import static Helper.HelperClass.contdFractionsPeriod;
import static Helper.HelperClass.getConvergent;
import static Helper.HelperClass.getTotient;
import static Helper.HelperClass.isSquare;
import static Helper.HelperClass.polygonalTypes;
import static Helper.HelperClass.sumOfDigits;
import static java.util.stream.Collectors.toList;

class Euler61To70 {

  static int problem61() {
    List<Integer> listOfPolygonals = new ArrayList<>();
    HashMap<Integer, List<Integer>> map = new HashMap<>();
    LongStream.range(3, 9).forEach(l -> map.put((int) l, new ArrayList<>()));
    for (int i = 1000; i < 10000; i++) {
      List<Integer> types = polygonalTypes(i);
      for (int j : types) {
        map.get(j).add(i);
      }
    }
    for (int i = 3; i < 9; i++) {
      listOfPolygonals.addAll(map.get(i));
    }
    Collections.sort(listOfPolygonals);
    for (Integer number : listOfPolygonals) {
      List<Integer> remainder = new ArrayList<>(listOfPolygonals);
      map.get(polygonalTypes(number).get(0)).forEach(remainder::remove);
      for (Integer number2 : remainder) {
        List<Integer> remainder2 = new ArrayList<>(remainder);
        if (String.valueOf(number2).substring(2, 4).equals(String.valueOf(number).substring(0, 2))) {
          map.get(polygonalTypes(number2).get(0)).forEach(remainder2::remove);
          for (Integer number3 : remainder2) {
            List<Integer> remainder3 = new ArrayList<>(remainder2);
            if (String.valueOf(number3).substring(2, 4).equals(String.valueOf(number2).substring(0, 2))) {
              map.get(polygonalTypes(number3).get(0)).forEach(remainder3::remove);
              for (Integer number4 : remainder3) {
                List<Integer> remainder4 = new ArrayList<>(remainder3);
                if (String.valueOf(number4).substring(2, 4).equals(String.valueOf(number3).substring(0, 2))) {
                  map.get(polygonalTypes(number4).get(0)).forEach(remainder4::remove);
                  for (Integer number5 : remainder4) {
                    List<Integer> remainder5 = new ArrayList<>(remainder4);
                    if (String.valueOf(number5).substring(2, 4).equals(String.valueOf(number4).substring(0, 2))) {
                      map.get(polygonalTypes(number5).get(0)).forEach(remainder5::remove);
                      for (Integer number6 : remainder5) {
                        if (String.valueOf(number6).substring(2, 4).equals(String.valueOf(number5).substring(0, 2)) &&
                            String.valueOf(number6).substring(0, 2).equals(String.valueOf(number).substring(2, 4)
                            )) {
                          return number + number2 + number3 + number4 + number5 + number6;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return -1;
  }

  static long problem62() {
    for (long i = 345L; i < 10000L; i++) {
      BigInteger cube = BigInteger.valueOf(i).pow(3);
      if (LongStream.range(i + 1, 10000).parallel().mapToObj(BigInteger::valueOf).map(B -> B.pow(3))
          .filter(B -> arePermutations(cube, B))
          .count() == 4) {
        return cube.longValue();
      }
    }
    return -1;
  }

  static long problem63() {
    return LongStream.range(1, 10).map(i ->
        IntStream.range(1, 1000).filter(j -> BigInteger.valueOf(i).pow(j).toString().length() == j).count()
    ).sum();
  }

  static long problem64() {
    return IntStream.range(2, 10001).filter(i -> !isSquare(i))
        .filter(i -> contdFractionsPeriod((int) Math.sqrt(i), i, (int) Math.sqrt(i), new ArrayList<>(), 1).size() % 2 != 0)
        .count();
  }

  static long problem65() {
    BigInteger denominator = BigInteger.ONE;
    BigInteger numerator = BigInteger.ONE;
    for (int i = 98; i > 0; i--) {
      BigInteger whole = i % 3 == 2 ? BigInteger.valueOf(i - i / 3) : BigInteger.ONE;
      BigInteger tmp = whole.multiply(denominator).add(numerator);
      numerator = denominator;
      denominator = tmp;
    }
    BigInteger finalNumerator = denominator.multiply(BigInteger.valueOf(2)).add(numerator);
    return sumOfDigits(finalNumerator);
  }

  static long problem66() {
    long D = 0;
    BigInteger maxX = BigInteger.ZERO;
    for (int d = 2; d <= 1000; d++) {
      BigInteger X = BigInteger.ZERO;
      if (!isSquare(d)) {
        List<Integer> list = contdFractionsPeriod((int) Math.sqrt(d), d, (int) Math.sqrt(d), new ArrayList<>(), 1);
        int counter = 1;
        while (true) {
          BigInteger[] array = getConvergent(list, counter, d);
          BigInteger x = array[0];
          BigInteger y = array[1];
          if (x.pow(2).subtract(y.pow(2).multiply(BigInteger.valueOf(d))).equals(BigInteger.ONE)) {
            X = x;
            break;
          }
          counter++;
        }
      }
      if (X.compareTo(maxX) > 0) {
        maxX = X;
        D = d;
      }
    }
    return D;
  }

  static int problem67() throws URISyntaxException, IOException {
    String triangle = new String(Files.readAllBytes(Paths.get(Euler61To70.class.getResource("/problem67.txt").toURI())));
    String[] lines = triangle.split("\\r?\\n");
    int[][] matrix = new int[lines.length][lines.length];
    for (int i = 0; i < lines.length; i++) {
      String[] line = lines[i].split(" ");
      for (int j = 0; j < line.length; j++) {
        matrix[i][j] = Integer.valueOf(line[j]);
      }
    }

    for (int i = matrix.length - 2; i >= 0; i--) {
      for (int j = 0; j < i + 1; j++) {
        if (matrix[i + 1][j] > matrix[i + 1][j + 1]) {
          matrix[i][j] = matrix[i][j] + matrix[i + 1][j];
        } else {
          matrix[i][j] = matrix[i][j] + matrix[i + 1][j + 1];
        }
      }
    }
    return matrix[0][0];
  }

  static long problem68() {
    Permuter permuter = new Permuter();
    permuter.permute("123456789");
    Long max = permuter.getPermutations().stream()
        .map(s -> IntStream.concat(IntStream.of(10), s.chars().map(Character::getNumericValue)).toArray())
        .filter(n ->
            IntStream.iterate(0, i -> i + 2).limit(4).allMatch(i -> n[i] + n[i + 1] == n[i + 2] + n[(i + 5) % n.length])
        ).map(n -> {
          int min = IntStream.iterate(0, j -> j + 2).limit(n.length / 2).map(j -> n[j]).min().orElse(0);
          final int[] offset = {IntStream.of(n).boxed().collect(toList()).indexOf(min)};
          StringBuilder b = new StringBuilder("");
          IntStream.range(1, 16).map(k -> (k % 3 == 0) ? n[(k + offset[0]--) % n.length] : n[(k - 1 + offset[0]) % n.length])
              .forEach(k -> b.append(Integer.toString(k)));
          return Long.valueOf(b.toString());
        }).collect(toList()).stream().mapToLong(l -> l).max().orElse(-1);
    return max;
  }

  static long problem69() {
    List<Double> divResults = LongStream.range(2, 1000001).parallel()
        .mapToDouble(i -> (double) i / getTotient(i))
        .boxed().collect(toList());
    return divResults.indexOf(divResults.stream().mapToDouble(d -> d).max().orElse(-1)) + 2;
  }

  //TODO: make it faster
  static long problem70() {
    final long[] n = {0};
    final double[] min = {10000000};
    LongStream.range(2, 10000000).parallel().forEach(i -> {
      double counter = getTotient(i);
      if (arePermutations((int) i, (int) counter)) {
        double division = (double) i / counter;
        if (division < min[0]) {
          min[0] = division;
          n[0] = i;
        }
      }
    });
    return n[0];
  }
}

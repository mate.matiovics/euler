package problems;

import Sudoku.Sudoku;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Euler96 {

  public static int solve() throws URISyntaxException, IOException {
    int sum = 0;
    String text = new String(Files.readAllBytes(Paths.get(Euler96.class.getResource("/problem96.txt").toURI())));
    String[] grids = text.replaceFirst("^Grid\\s\\d{2}\\r?\\n", "").split("Grid\\s\\d{2}\\r?\\n");
    for (String grid : grids) {
      Sudoku sudoku = new Sudoku(grid.split("\\r?\\n"));
      sudoku.solve();
      int[] values = sudoku.getFirstThreeNumbers();
      StringBuilder b = new StringBuilder();
      for (int i = 0; i < 3; i++) {
        b.append(values[i]);
      }
      sum += Integer.valueOf(b.toString());
    }
    return sum;
  }
}

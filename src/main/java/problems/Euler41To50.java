package problems;

import Helper.HelperClass;
import Helper.Permuter;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static Helper.HelperClass.alphabeticalValue;
import static Helper.HelperClass.arePermutations;
import static Helper.HelperClass.getPrimeFactors;
import static Helper.HelperClass.isHexagonalNo;
import static Helper.HelperClass.isPentagonalNo;
import static Helper.HelperClass.isPrime;
import static Helper.HelperClass.isTriangleNo;
import static Helper.HelperClass.substringsProducePrime;

class Euler41To50 {

  static long problem41() {
    Permuter permuter = new Permuter();
    StringBuilder builder = new StringBuilder("");
    IntStream.range(1, 10).forEach(i -> permuter.permute(builder.append(i)));
    return permuter.getPermutations().stream().mapToLong(Long::valueOf)
        .filter(HelperClass::isPrime).max().orElse(-1);
  }

  static long problem42() throws URISyntaxException, IOException {
    String fileContent = new String(Files.readAllBytes(Paths.get(Euler41To50.class.getResource("/problem42.txt").toURI())));
    return Arrays.stream(fileContent.replaceAll("\"", "").split(","))
        .filter(s -> isTriangleNo(alphabeticalValue(s))).count();
  }

  static long problem43() {
    Permuter permuter = new Permuter();
    permuter.permute(1234567890);
    return permuter.getPermutations().stream()
        .filter(s -> !s.startsWith("0") && substringsProducePrime(s)).mapToLong(Long::valueOf).sum();
  }

  static long problem44() {
    List<Long> pentagonals = LongStream.range(1, 1000000).mapToObj(i -> i * (3 * i - 1) / 2).collect(Collectors.toList());
    for (int i = 1; i < pentagonals.size(); i++) {
      for (int j = i - 1; j > 0; j--) {
        long difference = pentagonals.get(i) - pentagonals.get(j);
        long sum = pentagonals.get(i) + pentagonals.get(j);
        if (isPentagonalNo(sum) && isPentagonalNo(difference)) {
          return Math.abs(pentagonals.get(i) - pentagonals.get(j));
        }
      }
    }
    return -1;
  }

  static long problem45() {
    List<Long> triangles = LongStream.range(1, 1000000).mapToObj(i -> i * (i + 1) / 2).collect(Collectors.toList());
    return IntStream.range(286, triangles.size()).mapToLong(triangles::get)
        .filter(triangle -> isPentagonalNo(triangle) && isHexagonalNo(triangle))
        .findFirst().orElse(-1);
  }

  static long problem46() {
    return LongStream.range(9, 1000000).filter(i -> i % 2 != 0 && !isPrime(i) &&
        LongStream.range(1, i).noneMatch(j -> isPrime(j) && Math.sqrt((i - j) / 2.0) % 1.0 == 0))
        .findFirst().orElse(-1);
  }

  static long problem47() {
    return LongStream.range(3, 1000000).filter(i -> getPrimeFactors(i).size() == 4 && getPrimeFactors(i + 1).size() == 4 &&
        getPrimeFactors(i + 2).size() == 4 && getPrimeFactors(i + 3).size() == 4)
        .findFirst().orElse(-1);
  }

  static String problem48() {
    BigInteger sum = BigInteger.ZERO;
    for (long i = 1; i < 1001; i++) {
      sum = sum.add(BigInteger.valueOf(i).pow((int) i));
    }
    String string = sum.toString();
    return string.substring(string.length() - 10);
  }

  static String problem49() {
    for (int i = 1488; i < 10000; i++) {
      if (isPrime(i)) {
        for (int j = i + 1; j < 10000; j++) {
          int k = j + (j - i);
          if (isPrime(j) && isPrime(k) && arePermutations(i, j) && arePermutations(i, k)) {
            return String.valueOf(i) + String.valueOf(j) + String.valueOf(k);
          }
        }
      }
    }
    return "-1";
  }

  static int problem50() {
    int primeSum = 0;
    int max = 0;
    for (int i = 2; i < 1000; i++) {
      int prime = i;
      int counter = 0;
      int sum = 0;
      while (sum < 1000000) {
        if (isPrime(prime)) {
          sum += prime;
          counter++;
          if (isPrime(sum) && counter > max) {
            max = counter;
            primeSum = sum;
          }
        }
        prime++;
      }
    }
    return primeSum;
  }
}

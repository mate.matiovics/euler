package problems;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

//TODO: refactor
class Euler81To83 {

  static int problem81() throws URISyntaxException, IOException {
    int[][] matrix = initMatrix();
    return matrixPath(matrix);
  }

  static int problem82() throws URISyntaxException, IOException {
    int[][] matrix = initMatrix();
    int[][] calculated = new int[matrix.length][matrix[0].length];
    for (int a = 0; a < calculated.length; a++) {
      calculated[a][0] = matrix[a][0];
    }
    for (int j = 1; j < matrix.length; j++) {
      for (int i = 0; i < matrix[j].length; i++) {
        TreeSet<Integer> temp = new TreeSet<>();
        for (int k = 0; k < matrix.length; k++) {
          int[][] input;
          if (k <= i) {
            input = subMatrix(matrix, k, j - 1, i + 1, j + 1);
            input[0][0] = calculated[k][j - 1];
          } else {
            input = mirrorMatrix(subMatrix(matrix, i, j - 1, k + 1, j + 1));
            input[input.length - 1][1] = calculated[k][j - 1];
          }
          temp.add(matrixPath(input));
        }
        calculated[i][j] = temp.first();
      }
    }
    TreeSet<Integer> result = new TreeSet<>();
    for (int[] line : calculated) {
      result.add(line[calculated.length - 1]);
    }
    return result.first();
  }

  static int problem83() throws IOException, URISyntaxException {
    int[][] matrix = initMatrix();

    Node[][] nodes = new Node[matrix.length][matrix.length];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix.length; j++) {
        nodes[i][j] = new Node(i * matrix.length + j, matrix[i][j], 1000000000);
      }
    }
    for (int i = 0; i < nodes.length; i++) {
      for (int j = 0; j < nodes.length; j++) {
        if (i != 0) {
          nodes[i][j].adjacents.add(nodes[i - 1][j]);
        }
        if (i != nodes.length - 1) {
          nodes[i][j].adjacents.add(nodes[i + 1][j]);
        }
        if (j != 0) {
          nodes[i][j].adjacents.add(nodes[i][j - 1]);
        }
        if (j != nodes.length - 1) {
          nodes[i][j].adjacents.add(nodes[i][j + 1]);
        }
      }
    }

    List<Node> unvisited = new ArrayList<>();
    nodes[0][0].estimation = nodes[0][0].value;
    for (Node[] node : nodes) {
      unvisited.addAll(Arrays.asList(node));
    }
    unvisited.sort(Comparator.comparingInt(o -> o.estimation));

    while (!unvisited.isEmpty()) {
      Node current = unvisited.get(0);
      unvisited.remove(0);
      for (Node adjacent : current.adjacents) {
        if (current.estimation + adjacent.value < adjacent.estimation) {
          adjacent.estimation = current.estimation + adjacent.value;
        }
      }
      unvisited.sort(Comparator.comparingInt(o -> o.estimation));
    }
    return nodes[nodes.length - 1][nodes.length - 1].estimation;
  }

  private static int[][] initMatrix() throws URISyntaxException, IOException {
    String content = new String(Files.readAllBytes(Paths.get(Euler81To83.class.getResource("/problem81.txt").toURI())));
    String[] lines = content.split("\\r?\\n");
    int[][] matrix = new int[lines.length][lines.length];
    for (int i = 0; i < lines.length; i++) {
      String[] line = lines[i].split(",");
      for (int j = 0; j < line.length; j++) {
        matrix[i][j] = Integer.valueOf(line[j]);
      }
    }
    return matrix;
  }

  private static int matrixPath(int[][] matrix) {
    List<TreeSet<Integer>> list = new ArrayList<>();
    for (int i = 0; i < matrix.length * matrix[0].length; i++) {
      list.add(new TreeSet<>());
    }
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        if (i == 0 && j == 0) {
          list.get(0).add(matrix[0][0]);
        } else if (i == 0) {
          list.get(j).add(matrix[0][j] + list.get(j - 1).first());
        } else if (j == 0) {
          list.get(i * matrix[i].length).add(matrix[i][0] + list.get((i - 1) * matrix[i].length).first());
        } else {
          list.get(i * matrix[i].length + j).add(matrix[i][j] + list.get(i * matrix[i].length + j - 1).first());
          list.get(i * matrix[i].length + j).add(matrix[i][j] + list.get((i - 1) * matrix[i].length + j).first());
        }
      }
    }
    return list.get(matrix.length * matrix[0].length - 1).first();
  }

  private static int[][] subMatrix(int[][] matrix, int startI, int startJ, int endI, int endJ) {
    int[][] newMatrix = new int[endI - startI][endJ - startJ];
    for (int i = 0; i < newMatrix.length; i++) {
      for (int j = 0; j < newMatrix[i].length; j++) {
        newMatrix[i][j] = matrix[startI + i][startJ + j];
      }
    }
    return newMatrix;
  }

  private static int[][] mirrorMatrix(int[][] matrix) {
    int[][] newMatrix = new int[matrix.length][matrix[0].length];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        newMatrix[i][matrix[i].length - 1 - j] = matrix[i][j];
      }
    }
    return newMatrix;
  }

  private static class Node {
    int id;
    int value;
    int estimation;
    int sum = 0;
    List<Node> adjacents = new ArrayList<>();

    Node(int id, int value, int estimation) {
      this.id = id;
      this.value = value;
      this.estimation = estimation;
    }
  }
}

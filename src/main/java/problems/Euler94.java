package problems;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static Helper.HelperClass.contdFractionsPeriod;
import static Helper.HelperClass.getConvergent;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.valueOf;

public class Euler94 {

  public static long solve() {
    BigInteger P = valueOf(192);
    BigInteger P1 = valueOf(720);
    BigInteger Q = valueOf(28);
    BigInteger Q1 = valueOf(104);
    int d = 48;
    BigInteger D = valueOf(d);
    List<Integer> list = contdFractionsPeriod((int) Math.sqrt(d), d, (int) Math.sqrt(d), new ArrayList<>(), 1);
    int counter = 1;
    BigInteger sum = valueOf(16).add(valueOf(50));
    while (true) {
      BigInteger[] array = getConvergent(list, counter, d);
      BigInteger x = array[0];
      BigInteger y = array[1];

      if (x.pow(2).subtract(y.pow(2).multiply(D)).equals(BigInteger.ONE)) {
        BigInteger q = P.multiply(y).add(Q.multiply(x));
        BigInteger q1 = P1.multiply(y).add(Q1.multiply(x));

        BigInteger sideA = q.add(valueOf(2)).divide(valueOf(6));
        BigInteger sideA1 = q1.add(valueOf(2)).divide(valueOf(6));

        BigInteger sideB = sideA.add(ONE);
        BigInteger sideB1 = sideA1.subtract(ONE);

        BigInteger perim = sideA.multiply(valueOf(2)).add(sideB);
        BigInteger perim1 = sideA1.multiply(valueOf(2)).add(sideB1);

        if (perim.compareTo(valueOf(1000000000L)) > 0) {
          break;
        }

        sum = sum.add(perim);
        sum = sum.add(perim1);

      }
      counter++;
    }
    return sum.longValue();
  }
}

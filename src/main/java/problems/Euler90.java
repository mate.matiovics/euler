package problems;

import java.util.List;

import static Helper.HelperClass.getNCRCombinations;

class Euler90 {

  static long solve() {
    int result = 0;
    List<int[]> ncrCombinations = getNCRCombinations(10, 6);
    for (int[] a : ncrCombinations) {
      for (int[] b : ncrCombinations) {
        if (evalForSquares(a, b)) {
          result++;
        }
      }
    }
    return result / 2;
  }

  private static boolean evalForSquares(int[] die1, int[] die2) {
    int[] squares = {1, 4, 9, 16, 25, 36, 49, 64, 81};
    for (int square : squares) {
      int firstDigit = square / 10;
      int secondDigit = square % 10;
      if (!checkIfContains(die1, die2, firstDigit, secondDigit)) {
        return false;
      }
    }
    return true;
  }

  private static boolean checkIfContains(int[] die1, int[] die2, int digit1, int digit2) {
    return contains(die1, digit1) && contains(die2, digit2) || contains(die2, digit1) && contains(die1, digit2);
  }

  private static boolean contains(int[] die, int digit) {
    int substitute = digit;
    if (digit == 6)
      substitute = 9;
    if (digit == 9)
      substitute = 6;
    for (int i : die) {
      if (i == digit || i == substitute) {
        return true;
      }
    }
    return false;
  }
}

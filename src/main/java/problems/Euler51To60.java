package problems;

import Helper.HelperClass;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static Helper.HelperClass.arePermutations;
import static Helper.HelperClass.isPalindrome;
import static Helper.HelperClass.isPrime;
import static Helper.HelperClass.nCr;
import static Helper.HelperClass.primePairs;
import static Helper.HelperClass.replaceDigits;
import static Helper.HelperClass.reverseNumber;
import static Helper.HelperClass.sumOfDigits;
import static Poker.PokerHelper.compareHands;

class Euler51To60 {

  static long problem51() {
    for (int i = 1000; i < 1000000; i++) {
      List<List<String>> list = replaceDigits(i);
      Optional<List<String>> optional = list.stream()
          .filter(strings -> strings.stream()
              .filter(s -> isPrime(Long.valueOf(s)))
              .count() >= 8)
          .findFirst();
      if (optional.isPresent()) {
        return Long.valueOf(optional.get().get(0));
      }
    }
    return -1;
  }

  static int problem52() {
    return IntStream.range(1, 1000000)
        .filter(i -> IntStream.range(1, 7).allMatch(j -> arePermutations(i, j * i)))
        .findFirst().orElse(-1);
  }

  static long problem53() {
    return LongStream.range(5, 101)
        .map(i -> LongStream.range(2, i).map(j -> nCr(i, j)).filter(n -> n > 1000000 || n < 0).count())
        .sum();
  }

  static int problem54() throws URISyntaxException, IOException {
    String content = new String(Files.readAllBytes(Paths.get(Euler51To60.class.getResource("/problem54.txt").toURI())));
    String[] hands = content.replaceAll("\r", "").split("\n");
    List<String> player1hands = Stream.of(hands).map(s -> s.substring(0, 14)).collect(Collectors.toList());
    List<String> player2hands = Stream.of(hands).map(s -> s.substring(15, s.length())).collect(Collectors.toList());
    return (int) IntStream.range(0, player1hands.size())
        .filter(i -> compareHands(player1hands.get(i), player2hands.get(i)) == 1).count();
  }

  static int problem55() {
    return (int) IntStream.range(10, 10000).mapToObj(BigInteger::valueOf).filter(i -> {
      int counter = 1;
      while (counter < 51) {
        BigInteger reverse = reverseNumber(i);
        if (isPalindrome(i.add(reverse))) {
          return false;
        }
        i = i.add(reverse);
        counter++;
      }
      return true;
    }).count();
  }

  static long problem56() {
    return LongStream.range(1, 100)
        .map(i -> IntStream.range(2, 100)
            .mapToLong(j -> sumOfDigits(BigInteger.valueOf(i).pow(j))).max().orElse(-1))
        .max().orElse(-1);
  }

  static long problem57() {
    BigInteger numerator = BigInteger.ONE;
    BigInteger denominator = BigInteger.valueOf(2);
    long counter = 0;
    for (int i = 2; i < 1001; i++) {
      BigInteger tmp = numerator.add(denominator.multiply(BigInteger.valueOf(2)));
      numerator = denominator;
      denominator = tmp;
      if (String.valueOf(numerator.add(denominator)).length() > String.valueOf(denominator).length()) {
        counter++;
      }
    }
    return counter;
  }

  static long problem58() {
    long increment = 0;
    long number = 1;
    long totalCount = 1;
    long primeCount = 0;
    while (true) {
      increment += 2;
      for (int i = 0; i < 4; i++) {
        number += increment;
        if (isPrime(number)) {
          primeCount++;
        }
        totalCount++;
      }
      if ((double) primeCount / (double) totalCount < 0.1) {
        break;
      }
    }
    return increment + 1;
  }

  static long problem59() throws URISyntaxException, IOException {
    String characters = "abcdefghijklmnopqrstuvwxyz";
    List<String> permutations = new ArrayList<>();
    for (int i = 0; i < characters.length(); i++) {
      for (int j = 0; j < characters.length(); j++) {
        for (int k = 0; k < characters.length(); k++) {
          String string = "" + characters.charAt(i) + characters.charAt(j) +
              characters.charAt(k);
          permutations.add(string);
        }
      }
    }
    String content = new String(Files.readAllBytes(Paths.get(Euler51To60.class.getResource("/problem59.txt").toURI())))
        .replaceAll("\n", "").replaceAll("\r", "");
    String[] message = content.split(",");
    List<Integer> notAllowed = Arrays.asList(35, 36, 37, 38, 42, 43, 47, 60, 61, 62, 64, 91, 92, 93, 94, 95, 96);
    String key = permutations.stream()
        .filter(s -> IntStream.range(0, message.length).map(i -> Integer.valueOf(message[i]) ^ (int) s.charAt(i % 3))
            .noneMatch(d -> notAllowed.contains(d) || d < 32 || d > 122)).findFirst().orElse(null);
    return IntStream.range(0, message.length).map(i -> Integer.valueOf(message[i]) ^ (int) key.charAt(i % 3)).sum();
  }

  static long problem60() {
    List<Integer> primes = IntStream.range(3, 10000).filter(HelperClass::isPrime).boxed().collect(Collectors.toList());
    for (int i = 0; i < primes.size(); i++) {
      int a = primes.get(i);
      for (int j = i + 1; j < primes.size(); j++) {
        int b = primes.get(j);
        if (primePairs(a, b)) {
          for (int k = j + 1; k < primes.size(); k++) {
            int c = primes.get(k);
            if (primePairs(a, c) && primePairs(b, c)) {
              for (int l = k + 1; l < primes.size(); l++) {
                int d = primes.get(l);
                if (primePairs(a, d) && primePairs(b, d) && primePairs(c, d)) {
                  for (int m = l + 1; m < primes.size(); m++) {
                    int e = primes.get(m);
                    if (primePairs(a, e) && primePairs(b, e) && primePairs(c, e) &&
                        primePairs(d, e)) {
                      return (long) (a + b + c + d + e);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return -1;
  }

}

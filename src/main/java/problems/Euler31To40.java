package problems;

import Helper.HelperClass;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.LongStream;

import static Helper.HelperClass.commonDigits;
import static Helper.HelperClass.factorial;
import static Helper.HelperClass.getNthDigit;
import static Helper.HelperClass.greatestCommonDivisor;
import static Helper.HelperClass.isPalindrome;
import static Helper.HelperClass.isPanDigital;
import static Helper.HelperClass.removeCommonDigit;
import static Helper.HelperClass.toBinary;
import static java.lang.Character.getNumericValue;

class Euler31To40 {

  //TODO: refactor
  static long problem31() {
    long counter = 0;
    for (int a = 0; a <= 200; a++) {
      for (int b = 0; b <= 200 - a; b += 2) {
        for (int c = 0; c <= 200 - a - b; c += 5) {
          for (int d = 0; d <= 200 - a - b - c; d += 10) {
            for (int e = 0; e <= 200 - a - b - c - d; e += 20) {
              for (int f = 0; f <= 200 - a - b - c - d - e; f += 50) {
                for (int g = 0; g <= 200 - a - b - c - d - e - f; g += 100) {
                  for (int h = 0; h <= 200; h += 200) {
                    if (a + b + c + d + e + f + g + h == 200) {
                      counter++;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return counter;
  }

  static long problem32() {
    Set<Integer> set = new HashSet<>();
    for (int i = 1234; i < 9877; i++) {
      for (int j = 1; j <= i / 2; j++) {
        if (i % j == 0) {
          String string = String.valueOf(i) + String.valueOf(j) + String.valueOf(i / j);
          if (string.length() == 9 && isPanDigital(string)) {
            set.add(i);
          }
        }
      }
    }
    return set.stream().mapToLong(i -> i).sum();
  }

  static long problem33() {
    Map<Integer, Integer> fractions = new HashMap<>();
    for (int i = 11; i < 100; i++) {
      for (int j = 11; j < i; j++) {
        if (commonDigits(i, j) == 1 && (double) i / (double) j == removeCommonDigit(i, j)) {
          fractions.put(j, i);
        }
      }
    }
    long productNum = fractions.keySet().stream().reduce(1, Math::multiplyExact);
    long productDenom = fractions.keySet().stream().mapToLong(fractions::get).reduce(1, Math::multiplyExact);
    return productDenom / greatestCommonDivisor(new long[] {productNum, productDenom});
  }

  static long problem34() {
    return LongStream.range(3, 100000).filter(n ->
        String.valueOf(n).chars().mapToLong(c -> factorial(BigInteger.valueOf(getNumericValue(c))).longValue()).sum() == n)
        .sum();
  }

  static long problem35() {
    return LongStream.range(2, 1000000).filter(HelperClass::isPrime)
        .filter(HelperClass::isCircularPrime).count();
  }

  static long problem36() {
    return LongStream.range(1, 1000000).filter(HelperClass::isPalindrome)
        .filter(i -> isPalindrome(toBinary(i))).sum();
  }

  static long problem37() {
    return LongStream.range(10, 1000000).filter(HelperClass::isPrime)
        .filter(HelperClass::isTruncatablePrime).sum();
  }

  static long problem38() {
    return LongStream.range(1, 10000).mapToObj(i -> {
      StringBuilder builder = new StringBuilder("");
      int j = 1;
      while (builder.length() < 9) {
        builder.append(String.valueOf(i * j));
        j++;
      }
      return builder.toString();
    }).filter(s -> s.length() == 9)
        .mapToLong(Long::valueOf)
        .filter(HelperClass::isPanDigital).max().orElse(-1);
  }

  static int problem39() {
    int maxCounter = 0;
    int p = 0;
    for (int k = 0; k < 1001; k++) {
      int counter = 0;
      for (int i = 0; i < k; i++) {
        for (int j = i + 1; j < k - i; j++) {
          if (i * i + j * j == (k - i - j) * (k - i - j)) {
            counter++;
          }
        }
      }
      if (counter > maxCounter) {
        maxCounter = counter;
        p = k;
      }
    }
    return p;
  }

  static long problem40() {
    return LongStream.range(1, 7).map(i -> getNthDigit((int) Math.pow(10, i)))
        .reduce(1, Math::multiplyExact);
  }
}

package problems;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Euler89 {

  static long solve() throws URISyntaxException, IOException {
    String numerals = new String(Files.readAllBytes(Paths.get(Euler89.class.getResource("/problem89.txt").toURI())));
    String[] lines = numerals.split("\\r?\\n");
    return Arrays.stream(lines).mapToInt(n -> {
      long l = convertFromRomanNumeral(n);
      return n.length() - convertToRomanNumeral(l).length();
    }).sum();
  }

  private static long convertFromRomanNumeral(String roman) {
    Map<Character, Long> values = new HashMap<>();
    values.put('I', 1L);
    values.put('V', 5L);
    values.put('X', 10L);
    values.put('L', 50L);
    values.put('C', 100L);
    values.put('D', 500L);
    values.put('M', 1000L);
    char[] numerals = roman.toCharArray();
    int sum = 0;
    for (int i = 0; i < numerals.length; i++) {
      long current = values.get(numerals[i]);
      long next = 0;
      if (i != numerals.length - 1) {
        next = values.get(numerals[i + 1]);
      }
      if (next > current) {
        sum += (next - current);
        i++;
      } else {
        sum += current;
      }
    }
    return sum;
  }

  private static String convertToRomanNumeral(long n) {
    char[] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
    StringBuilder builder = new StringBuilder("");
    char[] digits = String.valueOf(n).toCharArray();
    for (int i = 0; i < digits.length; i++) {
      int magnitude = digits.length - i - 1;
      int value = Character.getNumericValue(digits[i]);
      if (magnitude >= 3) {
        for (int j = 0; j < value * Math.pow(10, magnitude - 3); j++) {
          builder.append(romanNumerals[romanNumerals.length - 1]);
        }
      } else {
        if (value == 9) {
          builder.append(romanNumerals[magnitude * 2]);
          builder.append(romanNumerals[(magnitude + 1) * 2]);
        } else if (value == 4) {
          builder.append(romanNumerals[magnitude * 2]);
          builder.append(romanNumerals[magnitude * 2 + 1]);
        } else if (value >= 5) {
          builder.append(romanNumerals[magnitude * 2 + 1]);
          for (int j = 0; j < value % 5; j++) {
            builder.append(romanNumerals[magnitude * 2]);
          }
        } else {
          for (int j = 0; j < value; j++) {
            builder.append(romanNumerals[magnitude * 2]);
          }
        }
      }
    }
    return builder.toString();
  }
}

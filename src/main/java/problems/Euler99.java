package problems;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Euler99 {

  public static int solve() throws URISyntaxException, IOException {
    String text = new String(Files.readAllBytes(Paths.get(Euler99.class.getResource("/problem99.txt").toURI())));
    String[] split = text.split("\\r?\\n");
    int counter = 1;
    int max = 0;
    int line = 0;
    for (String s : split) {
      String[] split1 = s.split(",");
      int v = (int) (Integer.parseInt(split1[1]) * Math.log(Integer.parseInt(split1[0])));
      if (v > max) {
        max = v;
        line = counter;
      }
      counter++;
    }
    return line;
  }
}

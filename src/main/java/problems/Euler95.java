package problems;

import java.util.ArrayList;
import java.util.List;

public class Euler95 {

  public static int solve() {
    int[] divisorSums = new int[1000001];
    for (int i = 1; i < 1000000 / 2; i++) {
      for (int j = i * 2; j < 1000000; j += i) {
        divisorSums[j] += i;
      }
    }

    boolean[] checked = new boolean[1000001];
    int max = 0;
    int minValue = 0;
    for (int i = 1; i < 1000001; i++) {
      if (!checked[i]) {
        List<Integer> chain = new ArrayList<>();
        chain.add(i);
        int sum = divisorSums[i];
        divisorSums[i] = sum;
        while (sum <= 1000000 && !checked[sum] && !chain.contains(sum)) {
          chain.add(sum);
          sum = divisorSums[sum];
        }
        checked[i] = true;
        if (sum <= 1000000 && chain.contains(sum)) {
          int count = chain.size() - chain.indexOf(sum);
          if (count > max) {
            max = count;
            minValue = sum;
          }
        }
        for (Integer integer : chain) {
          checked[integer] = true;
        }
      }
    }
    return minValue;
  }
}

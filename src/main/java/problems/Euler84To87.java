package problems;

import Monopoly.Monopoly;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static Helper.HelperClass.isPrime;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.math.BigInteger.valueOf;

class Euler84To87 {

  static String problem84() {
    Monopoly game = new Monopoly();
    Map<Integer, Long> map = game.play();
    StringBuilder b = new StringBuilder();
    for (int i = 0; i < 3; i++) {
      long max = 0;
      int maxKey = 0;
      for (Integer key : map.keySet()) {
        if (map.get(key) > max) {
          max = map.get(key);
          maxKey = key;
        }
      }
      b.append(maxKey);
      map.remove(maxKey);
    }
    return b.toString();
  }

  static int problem85() {
    TreeMap<Integer, Integer[]> map = new TreeMap<>();
    for (int i = 1; i * (i + 1) / 2 < 667000; i++) {
      int triangular = 2000000 / (i * (i + 1) / 2);
      int j = ((int) sqrt(8 * triangular + 1) - 1) / 2;
      int iTri = i * (i + 1) / 2;
      int jTri = j * (j + 1) / 2;
      int jPlusTri = (j + 1) * (j + 1 + 1) / 2;
      map.put(abs(iTri * jTri - 2000000), new Integer[]{i, j});
      map.put(abs(iTri * jPlusTri - 2000000), new Integer[]{i, j + 1});
    }
    Integer[] sides = map.get(map.firstKey());
    return sides[0] * sides[1];
  }

  static int problem86() {
    boolean[] isSquare = new boolean[(int) Math.pow(10000, 2)];
    for (int i = 0; i < 10000; i++) {
      isSquare[(int)(Math.pow(i, 2))] = true;
    }
    int counter = 0;
    for (int a = 1; a < 10000; a++) {
      double pow = pow(a, 2);
      for (int b = a; b > 0; b--) {
        for (int c = b; c > 0; c--) {
          if (isSquare[(int)(pow(b + c, 2) + pow)]) {
            counter++;
            if (counter > 1000000) {
              return a;
            }
          }
        }
      }
    }
    return -1;
  }

  static int problem87() {
    List<Integer> primes = new ArrayList<>();
    for (int i = 2; i < 7075; i++) {
      if (isPrime(i)) {
        primes.add(i);
      }
    }
    Set<BigInteger> set = new HashSet<>();
    for (Integer a : primes) {
      BigInteger aPow = valueOf(a).pow(2);
      for (Integer b : primes) {
        BigInteger bPow = valueOf(b).pow(3);
        if (aPow.add(bPow).compareTo(valueOf(50000000)) > 0) {
          break;
        }
        for (Integer c : primes) {
          BigInteger sum = aPow.add(bPow).add(valueOf(c).pow(4));
          if (sum.compareTo(valueOf(50000000)) < 0) {
            set.add(sum);
          } else {
            break;
          }
        }
      }
    }
    return set.size();
  }
}

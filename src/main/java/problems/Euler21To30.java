package problems;

import Helper.Permuter;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.LongStream;

import static Helper.HelperClass.alphabeticalValue;
import static Helper.HelperClass.nEqualskDigitSum;
import static Helper.HelperClass.primeCount;
import static Helper.HelperClass.sumOfDivisors;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.TEN;
import static java.math.BigInteger.valueOf;

class Euler21To30 {

  static long problem21() {
    Set<Long> checkedNumbers = new HashSet<>();
    for (long i = 1; i < 10000; i++) {
      long sumOfDivisors = sumOfDivisors(i);
      if (sumOfDivisors != i && sumOfDivisors(sumOfDivisors) == i) {
        checkedNumbers.add(i);
      }
    }
    return checkedNumbers.stream().mapToLong(i -> i).sum();
  }

  static long problem22() throws URISyntaxException, IOException {
    String content = new String(Files.readAllBytes(Paths.get(Euler21To30.class.getResource("/problem22.txt").toURI())));
    List<String> namesAsList = Arrays.asList(content.replaceAll("\"", "").split(","));
    Collections.sort(namesAsList);
    return namesAsList.stream().mapToLong(n -> alphabeticalValue(n) * (namesAsList.indexOf(n) + 1)).sum();
  }

  static long problem23() {
    Map<Long, Boolean> mapOfAbundants = new HashMap<>();
    return LongStream.range(1, 28124).filter(i -> {
      mapOfAbundants.put(i, sumOfDivisors(i) > i);
      return LongStream.range(1, i / 2 + 1).noneMatch(j -> mapOfAbundants.get(j) && mapOfAbundants.get(i - j));
    }).sum();
  }

  static String problem24() {
    Permuter permuter = new Permuter();
    permuter.permute("0123456789");
    List<String> permutations = permuter.getPermutations();
    Collections.sort(permutations);
    return permutations.get(999999);
  }

  static long problem25() {
    BigInteger limit = TEN.pow(999);
    BigInteger prev = ONE;
    BigInteger actual = valueOf(2);
    long counter = 3;
    while (actual.compareTo(limit) < 0) {
      BigInteger temp = actual;
      actual = prev.add(actual);
      prev = temp;
      counter++;
    }
    return counter;
  }

  static long problem26() {
    long maxSize = 0;
    long denominator = 0;
    for (long i = 1; i < 1000; i++) {
      String string = BigDecimal.valueOf(1).divide(BigDecimal.valueOf(i), 1000, BigDecimal.ROUND_HALF_UP).toString();
      String snippet = string.substring(2, 7);
      if (string.indexOf(snippet) != string.lastIndexOf(snippet)) {
        String without = string.substring(7);
        int endindex = without.indexOf(snippet) + 7;
        String substring = string.substring(string.indexOf(snippet), endindex);
        if (substring.length() > maxSize) {
          maxSize = substring.length();
          denominator = i;
        }
      }
    }
    return denominator;
  }

  static long problem27() {
    long maxCounter = 0;
    long product = 0;
    for (int a = -999; Math.abs(a) < 1000; a++) {
      for (int b = -1000; Math.abs(b) <= 1000; b++) {
        long primeCount = primeCount(a, b);
        if (primeCount >= maxCounter) {
          maxCounter = primeCount;
          product = a * b;
        }
      }
    }
    return product;
  }

  static long problem28() {
    long sum = 1;
    long spiralCount = 2;
    long currentNo = 1;
    long increment = 2;
    while (spiralCount <= 501) {
      for (int i = 0; i < 4; i++) {
        currentNo += increment;
        sum += currentNo;
      }
      increment += 2;
      spiralCount++;
    }
    return sum;
  }

  static int problem29() {
    Set<String> set = new HashSet<>();
    for (long i = 2; i < 101; i++) {
      for (long j = 2; j < 101; j++) {
        set.add(String.valueOf(Math.pow(i, j)));
      }
    }
    return set.size();
  }

  static long problem30() {
    return LongStream.range(10, 1000000).filter(i -> nEqualskDigitSum(i, 5)).sum();
  }
}

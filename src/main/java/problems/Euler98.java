package problems;

import Helper.HelperClass;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

public class Euler98 {
  public static int solve() throws URISyntaxException, IOException {
    int result = 0;

    String text = new String(Files.readAllBytes(Paths.get(Euler98.class.getResource("/problem98.txt").toURI())));
    String s = text.replaceAll("\"", "");
    String[] split = s.split(",");

    List<Pair> list = new ArrayList<>();
    for (int i = 0; i < split.length; i++) {
      for (int j = i + 1; j < split.length; j++) {
        if (anagrams(split[i], split[j])) {
          list.add(new Pair(split[i], split[j]));
        }
      }
    }

    for (Pair pair : list) {
      int letterCount = letterCount(pair.left);
      List<Integer> squares = generateSquares(pair.left.length()).stream()
          .filter(i -> letterCount(i.toString()) == letterCount)
          .collect(Collectors.toList());

      List<Pair> numbers = new ArrayList<>();
      for (int i = 0; i < squares.size(); i++) {
        for (int j = i + 1; j < squares.size(); j++) {
          if (HelperClass.arePermutations(squares.get(i), squares.get(j))) {
            numbers.add(new Pair(squares.get(i).toString(), squares.get(j).toString()));
          }
        }
      }
      for (Pair number : numbers) {
        if (compare(pair, number)) {
          int n1 = parseInt(number.left);
          int n2 = parseInt(number.right);
          if (n1 > result)
            result = n1;
          if (n2 > result)
            result = n2;
        }
      }
    }
    return result;
  }

  private static boolean compare(Pair word, Pair number) {
    Map<Character, Character> mappings = new HashMap<>();
    String leftWord = word.left;
    String leftNumber = number.left;
    for (int i = 0; i < leftWord.length(); i++) {
      mappings.put(leftWord.charAt(i), leftNumber.charAt(i));
    }
    StringBuilder b = new StringBuilder();
    String rightWord = word.right;
    String rightNumber = number.right;
    for (int i = 0; i < word.right.length(); i++) {
      b.append(mappings.get(rightWord.charAt(i)));
    }
    return b.toString().equals(rightNumber);
  }

  private static List<Integer> generateSquares(int n) {
    List<Integer> squares = new ArrayList<>();
    for (int i = (int) Math.sqrt(Math.pow(10, n - 1)); i <= Math.sqrt(Math.pow(10, n)); i++) {
      squares.add(i * i);
    }
    return squares;
  }

  private static int letterCount(String string) {
    Set<Character> set = new HashSet<>();
    for (char c : string.toCharArray()) {
      set.add(c);
    }
    return set.size();
  }

  private static boolean anagrams(String a, String b) {
    if (a.length() != b.length()) {
      return false;
    }
    char[] chars = a.toCharArray();
    Arrays.sort(chars);
    char[] chars1 = b.toCharArray();
    Arrays.sort(chars1);
    return Arrays.equals(chars, chars1);
  }

  private static class Pair {
    private String left;
    private String right;

    Pair(String left, String right) {
      this.left = left;
      this.right = right;
    }

    public String toString() {
      return String.format("[%s, %s]", left, right);
    }
  }
}

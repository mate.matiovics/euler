package problems;

import Helper.Interpreter;
import Helper.Permuter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import static Helper.HelperClass.getNCRCombinations;

public class Euler93 {
  private static final char[] OPERATORS = {'+', '-', '*', '/'};
  private static final int[] DIGITS = {1, 2, 3, 4, 5, 6, 7, 8, 9};

  public static String solve() {
    List<char[]> operatorPermutations = permuteOperators();
    List<int[]> parentheses = permuteParentheses();

    int max = 0;
    String finalResult = "";

    for (int[] digitCombo : getNCRCombinations(DIGITS.length, 4)) {
      TreeSet<Integer> results = new TreeSet<>();

      Permuter permuter = new Permuter();
      permuter.permuteIndices(digitCombo);

      for (int[] digitSet : permuter.getPermutedIndices()) {
        for (int[] parenthesisSet : parentheses) {
          for (char[] operatorSet : operatorPermutations) {
            String expression = combine(digitSet, operatorSet, parenthesisSet);

            Interpreter interpreter = new Interpreter();
            interpreter.parse(expression);
            Double result = interpreter.evaluate();

            if (result >= 0 && result % 1.0 == 0.0) {
              results.add(result.intValue());
            }
          }
        }
      }

      Integer largestNo = getLargestConsecutive(results);
      if (largestNo > max) {
        max = largestNo;
        int[] resultSet = Arrays.stream(digitCombo).map(i -> DIGITS[i]).toArray();
        finalResult = merge(resultSet);
      }
    }

    return finalResult;
  }

  private static Integer getLargestConsecutive(TreeSet<Integer> set) {
    int prev = set.first();
    for (Integer number : set) {
      if (number - prev > 1) {
        break;
      }
      prev = number;
    }
    return prev;
  }

  private static List<char[]> permuteOperators() {
    List<char[]> combinedOps = new ArrayList<>();

    for (char OPERATOR : OPERATORS) {
      for (char OPERATOR1 : OPERATORS) {
        for (char OPERATOR2 : OPERATORS) {
          combinedOps.add(new char[] {OPERATOR, OPERATOR1, OPERATOR2});
        }
      }
    }

    return combinedOps;
  }

  private static List<int[]> permuteParentheses() {
    List<int[]> parentheses = new ArrayList<>();

    parentheses.add(new int[] {0, 4});
    parentheses.add(new int[] {2, 6});
    parentheses.add(new int[] {4, 8});
    parentheses.add(new int[] {0, 6});
    parentheses.add(new int[] {2, 8});
    parentheses.add(new int[] {0, 4, 6, 10});
    parentheses.add(new int[] {0, 6, 1, 5});
    parentheses.add(new int[] {0, 6, 3, 7});
    parentheses.add(new int[] {2, 8, 3, 7});
    parentheses.add(new int[] {2, 8, 5, 9});

    return parentheses;
  }

  private static String merge(int[] digits) {
    Arrays.sort(digits);
    StringBuilder b = new StringBuilder();
    for (int digit : digits) {
      b.append(digit);
    }
    return b.toString();
  }

  private static String combine(int[] indices, char[] operators, int[] parentheses) {
    StringBuilder b = new StringBuilder();
    for (int i = 0; i < indices.length; i++) {
      int index = indices[i];
      b.append(DIGITS[index]);
      if (i < operators.length) {
        b.append(operators[i]);
      }
    }
    for (int i = 0; i < parentheses.length; i++) {
      b.insert(parentheses[i], i % 2 == 0 ? '(' : ')');
    }
    return b.toString();
  }
}

package problems;

class Euler92 {

  static int solve() {
    int result = 0;
    for (int i = 1; i < 10000000; i++) {
      int current = i;
      while (current != 1 && current != 89) {
        current = calculateNext(current);
      }
      if (current == 89) {
        result++;
      }
    }
    return result;
  }

  static private int calculateNext(int n) {
    int next = 0;
    char[] asArray = String.valueOf(n).toCharArray();
    for (char c : asArray) {
      next += Math.pow(Character.getNumericValue(c), 2);
    }
    return next;
  }
}

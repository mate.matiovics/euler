package Monopoly;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Monopoly {
  private List<String> CC = new ArrayList<>();
  private List<String> CH = new ArrayList<>();
  private Map<Integer, Long> board = new HashMap<>();
  private List<Integer> duds;

  public Monopoly() {
    for (int j = 0; j < 40; j++) {
      board.put(j, 0L);
    }
    for (int i = 0; i < 14; i++) {
      CC.add("X");
      if (i < 6) {
        CH.add("X");
      }
    }
    CC.add("GO");
    CC.add("JAIL");
    CH.add("GO");
    CH.add("JAIL");
    CH.add("C1");
    CH.add("E3");
    CH.add("H2");
    CH.add("R1");
    CH.add("R");
    CH.add("R");
    CH.add("U");
    CH.add("-3");
    duds = new ArrayList<>(board.keySet());
    duds.remove(Integer.valueOf(2));
    duds.remove(Integer.valueOf(7));
    duds.remove(Integer.valueOf(17));
    duds.remove(Integer.valueOf(22));
    duds.remove(Integer.valueOf(30));
    duds.remove(Integer.valueOf(33));
    duds.remove(Integer.valueOf(36));
  }

  private int rollDie() {
    return (int) (Math.random() * 4) + 1;
  }

  public Map<Integer, Long> play() {
    Collections.shuffle(CC);
    Collections.shuffle(CH);
    int position = 0;
    boolean dubs;
    boolean prevDubs = false;
    boolean prevPrevDubs = false;
    int counter = 0;
    int die1;
    int die2;
    while (counter < 1000000) {
      counter++;
      die1 = rollDie();
      die2 = rollDie();
      dubs = die1 == die2;
      if (dubs && prevDubs && prevPrevDubs) {
        prevDubs = false;
        position = 10;
      } else {
        position = (position + die1 + die2) % board.size();
        label:
        while (!duds.contains(position)) {
          if (position == 30) {
            position = 10;
          } else if (position == 2 || position == 17 || position == 33) {
            String card = CC.get(0);
            CC.remove(0);
            CC.add(card);
            switch (card) {
              case "GO":
                position = 0;
                break;
              case "JAIL":
                position = 10;
                break;
              default:
                break label;
            }
          } else if (position == 7 || position == 22 || position == 36) {
            String card = CH.get(0);
            CH.remove(0);
            CH.add(card);
            switch (card) {
              case "GO":
                position = 0;
                break;
              case "JAIL":
                position = 10;
                break;
              case "C1":
                position = 11;
                break;
              case "E3":
                position = 24;
                break;
              case "H2":
                position = 39;
                break;
              case "R1":
                position = 5;
                break;
              case "-3":
                position -= 3;
                break;
              case "R":
                if (position == 7) {
                  position = 15;
                } else if (position == 22) {
                  position = 25;
                } else {
                  position = 5;
                }
                break;
              case "U":
                if (position == 7) {
                  position = 12;
                } else if (position == 22) {
                  position = 28;
                } else {
                  position = 12;
                }
                break;
              default:
                break label;
            }
          }
        }
      }
      prevPrevDubs = prevDubs;
      prevDubs = dubs;
      long value = board.get(position);
      board.put(position, value + 1);
    }
    return board;
  }
}

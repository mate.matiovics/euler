package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler96Test {

  @Test
  public void solve() throws Exception {
    int actual = Euler96.solve();
    int expected = 24702;
    assertEquals(expected, actual);
  }

}

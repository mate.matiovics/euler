package problems;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Euler31To40Test {

  @Test
  public void problem31() {
    long expected = 73682;
    long actual = Euler31To40.problem31();
    assertEquals(expected, actual);
  }

  @Test
  public void problem32() {
    long expected = 45228;
    long actual = Euler31To40.problem32();
    assertEquals(expected, actual);
  }

  @Test
  public void problem33() {
    long expected = 100;
    long actual = Euler31To40.problem33();
    assertEquals(expected, actual);
  }

  @Test
  public void problem34() {
    long expected = 40730;
    long actual = Euler31To40.problem34();
    assertEquals(expected, actual);
  }

  @Test
  public void problem35() {
    long expected = 55;
    long actual = Euler31To40.problem35();
    assertEquals(expected, actual);
  }

  @Test
  public void problem36() {
    long expected = 872187;
    long actual = Euler31To40.problem36();
    assertEquals(expected, actual);
  }

  @Test
  public void problem37() {
    long expected = 748317;
    long actual = Euler31To40.problem37();
    assertEquals(expected, actual);
  }

  @Test
  public void problem38() {
    long expected = 932718654L;
    long actual = Euler31To40.problem38();
    assertEquals(expected, actual);
  }

  @Test
  public void problem39() {
    int expected = 840;
    int actual = Euler31To40.problem39();
    assertEquals(expected, actual);
  }

  @Test
  public void problem40() {
    long expected = 210;
    long actual = Euler31To40.problem40();
    assertEquals(expected, actual);
  }

}

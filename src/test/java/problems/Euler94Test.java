package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler94Test {

    @Test
    public void solve() {
        long actual = Euler94.solve();
        long expected = 518408346;
        assertEquals(expected, actual);
    }
}

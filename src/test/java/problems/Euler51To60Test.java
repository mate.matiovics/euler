package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler51To60Test {

  @Test
  public void problem51() {
    long expected = 121313;
    long actual = Euler51To60.problem51();
    assertEquals(expected, actual);
  }

  @Test
  public void problem52() {
    int expected = 142857;
    int actual = Euler51To60.problem52();
    assertEquals(expected, actual);
  }

  @Test
  public void problem53() {
    long expected = 4075;
    long actual = Euler51To60.problem53();
    assertEquals(expected, actual);
  }

  @Test
  public void problem54() throws IOException, URISyntaxException {
    int expected = 376;
    int actual = Euler51To60.problem54();
    assertEquals(expected, actual);
  }

  @Test
  public void problem55() {
    int expected = 249;
    int actual = Euler51To60.problem55();
    assertEquals(expected, actual);
  }

  @Test
  public void problem56() {
    long expected = 972;
    long actual = Euler51To60.problem56();
    assertEquals(expected, actual);
  }

  @Test
  public void problem57() {
    long expected = 153;
    long actual = Euler51To60.problem57();
    assertEquals(expected, actual);
  }

  @Test
  public void problem58() {
    long expected = 26241;
    long actual = Euler51To60.problem58();
    assertEquals(expected, actual);
  }

  @Test
  public void problem59() throws IOException, URISyntaxException {
    long expected = 107359;
    long actual = Euler51To60.problem59();
    assertEquals(expected, actual);
  }

  @Test
  public void problem60() {
    long expected = 26033;
    long actual = Euler51To60.problem60();
    assertEquals(expected, actual);
  }

}

package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler92Test {

  @Test
  public void solve() {
    int actual = Euler92.solve();
    int expected = 8581146;
    assertEquals(expected, actual);
  }
}

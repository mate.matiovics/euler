package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler61To70Test {

  @Test
  public void problem61() {
    int expected = 28684;
    int actual = Euler61To70.problem61();
    assertEquals(expected, actual);
  }

  @Test
  public void problem62() {
    long expected = 127035954683L;
    long actual = Euler61To70.problem62();
    assertEquals(expected, actual);
  }

  @Test
  public void problem63() {
    long expected = 49;
    long actual = Euler61To70.problem63();
    assertEquals(expected, actual);
  }

  @Test
  public void problem64() {
    long expected = 1322;
    long actual = Euler61To70.problem64();
    assertEquals(expected, actual);
  }

  @Test
  public void problem65() {
    long expected = 272;
    long actual = Euler61To70.problem65();
    assertEquals(expected, actual);
  }

  @Test
  public void problem66() {
    long expected = 661;
    long actual = Euler61To70.problem66();
    assertEquals(expected, actual);
  }

  //possibly needs EOL conversion on input file
  @Test
  public void problem67() throws IOException, URISyntaxException {
    int expected = 7273;
    int actual = Euler61To70.problem67();
    assertEquals(expected, actual);
  }

  @Test
  public void problem68() {
    long expected = 6531031914842725L;
    long actual = Euler61To70.problem68();
    assertEquals(expected, actual);
  }

  @Test
  public void problem69() {
    long expected = 510510;
    long actual = Euler61To70.problem69();
    assertEquals(expected, actual);
  }

  @Test
  public void problem70() {
    long expected = 8319823;
    long actual = Euler61To70.problem70();
    assertEquals(expected, actual);
  }
}

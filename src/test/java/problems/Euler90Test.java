package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler90Test {

  @Test
  public void solve() {
    long actual = Euler90.solve();
    long expected = 1217;
    assertEquals(expected, actual);
  }

}

package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler84To87Test {

  @Test
  public void problem84() {
    String actual = Euler84To87.problem84();
    String expected = "101524";
    assertEquals(expected, actual);
  }

  @Test
  public void problem85() {
    int actual = Euler84To87.problem85();
    int expected = 2772;
    assertEquals(expected, actual);
  }

  @Test
  public void problem86() {
    int actual = Euler84To87.problem86();
    int expected = 1818;
    assertEquals(expected, actual);
  }

  @Test
  public void problem87() {
    int actual = Euler84To87.problem87();
    int expected = 1097343;
    assertEquals(expected, actual);
  }
}

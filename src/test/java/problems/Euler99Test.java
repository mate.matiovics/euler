package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler99Test {

  @Test
  public void solve() throws Exception {
    int actual = Euler99.solve();
    int expected = 709;
    assertEquals(expected, actual);
  }

}

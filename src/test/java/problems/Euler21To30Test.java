package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler21To30Test {

  @Test
  public void problem21() {
    long expected = 31626;
    long actual = Euler21To30.problem21();
    assertEquals(expected, actual);
  }

  @Test
  public void problem22() throws IOException, URISyntaxException {
    long expected = 871198282L;
    long actual = Euler21To30.problem22();
    assertEquals(expected, actual);
  }

  @Test
  public void problem23() {
    long expected = 4179871L;
    long actual = Euler21To30.problem23();
    assertEquals(expected, actual);
  }

  @Test
  public void problem24() {
    String expected = "2783915460";
    String actual = Euler21To30.problem24();
    assertEquals(expected, actual);
  }

  @Test
  public void problem25() {
    long expected = 4782;
    long actual = Euler21To30.problem25();
    assertEquals(expected, actual);
  }

  @Test
  public void problem26() {
    long expected = 983;
    long actual = Euler21To30.problem26();
    assertEquals(expected, actual);
  }

  @Test
  public void problem27() {
    long expected = -59231;
    long actual = Euler21To30.problem27();
    assertEquals(expected, actual);
  }

  @Test
  public void problem28() {
    long expected = 669171001L;
    long actual = Euler21To30.problem28();
    assertEquals(expected, actual);
  }

  @Test
  public void problem29() {
    int expected = 9183;
    int actual = Euler21To30.problem29();
    assertEquals(expected, actual);
  }

  @Test
  public void problem30() {
    long expected = 443839;
    long actual = Euler21To30.problem30();
    assertEquals(expected, actual);
  }
}

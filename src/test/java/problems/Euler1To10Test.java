package problems;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Euler1To10Test {

  @Test
  public void problem1() {
    int expected = 233168;
    int actual = Euler1To10.problem1();
    assertEquals(expected, actual);
  }

  @Test
  public void problem2() {
    int expected = 4613732;
    int actual = Euler1To10.problem2();
    assertEquals(expected, actual);
  }

  @Test
  public void problem3() {
    long expected = 6857;
    long actual = Euler1To10.problem3();
    assertEquals(expected, actual);
  }

  @Test
  public void problem4() {
    int expected = 906609;
    int actual = Euler1To10.problem4();
    assertEquals(expected, actual);
  }

  @Test
  public void problem5() {
    int expected = 232792560;
    int actual = Euler1To10.problem5();
    assertEquals(expected, actual);
  }

  @Test
  public void problem6() {
    long expected = 25164150;
    long actual = Euler1To10.problem6();
    assertEquals(expected, actual);
  }

  @Test
  public void problem7() {
    long expected = 104743;
    long actual = Euler1To10.problem7();
    assertEquals(expected, actual);
  }

  @Test
  public void problem8() {
    long expected = 23514624000L;
    long actual = Euler1To10.problem8();
    assertEquals(expected, actual);
  }

  @Test
  public void problem9() {
    int expected = 31875000;
    int actual = Euler1To10.problem9();
    assertEquals(expected, actual);
  }

  @Test
  public void problem10() {
    long expected = 142913828922L;
    long actual = Euler1To10.problem10();
    assertEquals(expected, actual);
  }

}

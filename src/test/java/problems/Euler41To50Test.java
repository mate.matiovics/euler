package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler41To50Test {

  @Test
  public void problem41() {
    long expected = 7652413;
    long actual = Euler41To50.problem41();
    assertEquals(expected, actual);
  }

  @Test
  public void problem42() throws IOException, URISyntaxException {
    long expected = 162;
    long actual = Euler41To50.problem42();
    assertEquals(expected, actual);
  }

  @Test
  public void problem43() {
    long expected = 16695334890L;
    long actual = Euler41To50.problem43();
    assertEquals(expected, actual);
  }

  @Test
  public void problem44() {
    long expected = 5482660;
    long actual = Euler41To50.problem44();
    assertEquals(expected, actual);
  }

  @Test
  public void problem45() {
    long expected = 1533776805L;
    long actual = Euler41To50.problem45();
    assertEquals(expected, actual);
  }

  @Test
  public void problem46() {
    long expected = 5777;
    long actual = Euler41To50.problem46();
    assertEquals(expected, actual);
  }

  @Test
  public void problem47() {
    long expected = 134043;
    long actual = Euler41To50.problem47();
    assertEquals(expected, actual);
  }

  @Test
  public void problem48() {
    String expected = "9110846700";
    String actual = Euler41To50.problem48();
    assertEquals(expected, actual);
  }

  @Test
  public void problem49() {
    String expected = "296962999629";
    String actual = Euler41To50.problem49();
    assertEquals(expected, actual);
  }

  @Test
  public void problem50() {
    int expected = 997651;
    int actual = Euler41To50.problem50();
    assertEquals(expected, actual);
  }

}

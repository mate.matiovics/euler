package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler91Test {

  @Test
  public void solve() {
    long actual = Euler91.solve();
    long expected = 14234;
    assertEquals(expected, actual);
  }
}

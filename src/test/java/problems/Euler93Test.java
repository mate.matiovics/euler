package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler93Test {

    @Test
    public void solve() {
        String actual = Euler93.solve();
        String expected = "1258";
        assertEquals(expected, actual);
    }
}

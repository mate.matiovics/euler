package problems;

import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler11To20Test {

  @Test
  public void problem11() {
    long expected = 70600674;
    long actual = Euler11To20.problem11();
    assertEquals(expected, actual);
  }

  @Test
  public void problem12() {
    long expected = 76576500;
    long actual = Euler11To20.problem12();
    assertEquals(expected, actual);
  }

  @Test
  public void problem13() throws IOException, URISyntaxException {
    BigInteger expected = BigInteger.valueOf(5537376230L);
    BigInteger actual = Euler11To20.problem13();
    assertEquals(expected, actual);
  }

  @Test
  public void problem14() {
    long expected = 837799;
    long actual = Euler11To20.problem14();
    assertEquals(expected, actual);
  }

  @Test
  public void problem15() {
    long expected = 137846528820L;
    long actual = Euler11To20.problem15();
    assertEquals(expected, actual);
  }

  @Test
  public void problem16() {
    long expected = 1366;
    long actual = Euler11To20.problem16();
    assertEquals(expected, actual);
  }

  @Test
  public void problem17() {
    int expected = 21124;
    int actual = Euler11To20.problem17();
    assertEquals(expected, actual);
  }

  @Test
  public void problem18() throws IOException, URISyntaxException {
    int expected = 1074;
    int actual = Euler11To20.problem18();
    assertEquals(expected, actual);
  }

  @Test
  public void problem19() {
    int expected = 171;
    int actual = Euler11To20.problem19();
    assertEquals(expected, actual);
  }

  @Test
  public void problem20() {
    long expected = 648;
    long actual = Euler11To20.problem20();
    assertEquals(expected, actual);
  }

}

package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler89Test {

  @Test
  public void solve() throws Exception {
    long actual = Euler89.solve();
    long expected = 743;
    assertEquals(expected, actual);
  }

}

package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class Euler71To80Test {

  @Test
  public void problem71() {
    long actual = Euler71To80.problem71();
    long expected = 428570;
    assertEquals(expected, actual);
  }

  @Test
  public void problem72() {
    long actual = Euler71To80.problem72();
    long expected = 303963552391L;
    assertEquals(expected, actual);
  }

  @Test
  public void problem73() {
    long actual = Euler71To80.problem73();
    long expected = 7295372;
    assertEquals(expected, actual);
  }

  @Test
  public void problem74() {
    long actual = Euler71To80.problem74();
    long expected = 402;
    assertEquals(expected, actual);
  }

  @Test
  public void problem75() {
    long actual = Euler71To80.problem75();
    long expected = 161667;
    assertEquals(expected, actual);
  }

  @Test
  public void problem76() {
    long actual = Euler71To80.problem76();
    long expected = 190569291;
    assertEquals(expected, actual);
  }

  @Test
  public void problem77() {
    int actual = Euler71To80.problem77();
    int expected = 71;
    assertEquals(expected, actual);
  }

  @Test
  public void problem78() {
    int actual = Euler71To80.problem78();
    int expected = 55374;
    assertEquals(expected, actual);
  }

  @Test
  public void problem79() throws IOException, URISyntaxException {
    String actual = Euler71To80.problem79();
    String expected = "73162890";
    assertEquals(expected, actual);
  }

  @Test
  public void problem80() {
    long actual = Euler71To80.problem80();
    long expected = 40886;
    assertEquals(expected, actual);
  }
}

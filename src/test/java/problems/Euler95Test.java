package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler95Test {

  @Test
  public void solve() {
    int actual = Euler95.solve();
    int expected = 14316;
    assertEquals(expected, actual);
  }
}

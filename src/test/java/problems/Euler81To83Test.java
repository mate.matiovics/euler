package problems;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

public class Euler81To83Test {

  @Test
  public void problem81() throws Exception {
    int actual = Euler81To83.problem81();
    int expected = 427337;
    assertEquals(expected, actual);
  }

  @Test
  public void problem82() throws IOException, URISyntaxException {
    int actual = Euler81To83.problem82();
    int expected = 260324;
    assertEquals(expected, actual);
  }

  @Test
  public void problem83() throws IOException, URISyntaxException {
    int actual = Euler81To83.problem83();
    int expected = 425185;
    assertEquals(expected, actual);
  }

}

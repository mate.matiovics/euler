package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler98Test {

  @Test
  public void solve() throws Exception {
    int actual = Euler98.solve();
    int expected = 18769;
    assertEquals(expected, actual);
  }

}

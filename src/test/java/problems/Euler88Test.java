package problems;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Euler88Test {

  @Test
  public void solve() {
    int expected = 7587457;
    int actual = Euler88.solve();
    assertEquals(expected, actual);
  }

}

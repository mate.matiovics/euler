package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler97Test {

  @Test
  public void solve() {
    String actual = Euler97.solve();
    String expected = "8739992577";
    assertEquals(expected, actual);
  }

}

package problems;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler100Test {

  @Test
  public void solve() {
    long actual = Euler100.solve();
    long expected = 756872327473L;
    assertEquals(expected, actual);
  }

}
